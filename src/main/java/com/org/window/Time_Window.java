package com.org.window;

import com.org.bean.SensorReading;
import org.apache.flink.api.common.functions.AggregateFunction;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.assigners.EventTimeSessionWindows;
import org.apache.flink.streaming.api.windowing.assigners.TumblingProcessingTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;

/**
 * @Authror: jcshen
 * @Date: 2020/12/27
 * @Version: 1.0
 * @Description:    window类型
 *      时间窗口（Time Window）：
 *          滚动时间窗口（Tumbling Windows）
 *              将数据依据固定的窗口长度对数据进行切分
 *              时间对齐，窗口长度固定，没有重叠
 *              window size;time
 *          滑动时间窗口（Sliding Windows）
 *               滑动窗口是固定窗口的更广义的一种行式，滑动窗口由固定的窗口长度和滑动间隔组成
 *               窗口长度固定，可以有重叠
 *               window size（窗口长度）;window slide（窗口步长）;time
 *          会话窗口（Session Windows）
 *              由一系列事件组合一个指定时间长度的timeout间隙组成，也就是一段时间没有接收到新数据就会生成新的窗口
 *              特点：时间无对齐
 *
 *      计数窗口（Count Window）
 *          滚动计数窗口
 *          滑动计数窗口
 *
 *      窗口分配器——window()方法
 *          用window()来定义一个窗口，然后基于这个window去做一些聚合或者其他处理操作。
 *          注意window()方法必须在keyBy之后才能用。
 *          Flink提供了 .timeWindow和 .countWindow方法，用于定义事件窗口和计数窗口。
 *
 *          window()方法接收的输入参数是一个WindowAssigner；
 *          windowAssigner负责将每条输入的数据分发到正确的window中；
 *          Flink提供了通用的WindowAssigner：
 *          滚动窗口：tumbling window——>调用：timeWindow(Time.seconds(15))
 *          滑动窗口：sliding window——>调用：timeWindow(Time.seconds(15),Time.seconds(5))
 *          会话窗口：session window——>调用：window(EventTimeSessionWindows.withGap(Time.minutes(1)))
 *          全局窗口：global window
 *          滚动计数窗口：tumbling count window——>调用：countWindow(5)
 *          滑动计数窗口：sliding count window——>调用：countWindow(10,2)
 *
 *     窗口函数（window function）
 *          window function定义了要对窗口中收集的数据做的计算操作。
 *          可以分为两类：
 *          1、增量聚合函数（incremental aggregation function）
 *              每条数据到来就进行计算，保持一个简单的状态；
 *              ReduceFunction AggregateFunction
 *
 *          2、全窗口函数（full window functions）
 *              先把窗口所有数据收集起来，等到计算的时候会遍历所有数据
 *              ProcessWindowFunctioin;WindowFunction
 *
 *
 *
 */
public class Time_Window {
    /**
     *  1、nc -lk 7777
     *  2、启动程序
     *  3、输入sensor的数据
     */
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        //从文件读取数据
//        DataStream<String> inputStream = env.readTextFile("F:\\workspace\\flink\\flink-11-java\\src\\main\\resources\\sensor.txt");
        //socket文本流
        DataStream<String> inputStream = env.socketTextStream("localhost", 7777);
        DataStream<SensorReading> dataStrem = inputStream.map(line -> {
            String[] fields = line.split(",");
            return new SensorReading(fields[0], new Long(fields[1]), new Double(fields[2]));
        });
        //开窗测试
        //1、增量聚合函数
        dataStrem.keyBy("id")
//                .timeWindow(Time.seconds(15),Time.seconds(5));//滑动窗口
//                .countWindow(10,2);//计数滑动窗口
//                .window(EventTimeSessionWindows.withGap(Time.minutes(1)));//会话窗口
                .timeWindow(Time.seconds(15))//滚动窗口
//                .window(TumblingProcessingTimeWindows.of(Time.seconds(15)))
                .aggregate(new AggregateFunction<SensorReading, Integer, Integer>() {

                    //创建累加器
                    @Override
                    public Integer createAccumulator() {
                        return 0;
                    }

                    //计数器如何累加
                    @Override
                    public Integer add(SensorReading value, Integer accumulator) {
                        return accumulator + 1;
                    }

                    //结果就是当前累加器
                    @Override
                    public Integer getResult(Integer accumulator) {
                        return accumulator;
                    }

                    //合并状态
                    @Override
                    public Integer merge(Integer a, Integer b) {
                        return a + b;
                    }
                }).print();

        env.execute("window job");

    }
}