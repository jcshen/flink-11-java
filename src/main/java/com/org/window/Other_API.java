package com.org.window;

import com.org.bean.SensorReading;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.util.OutputTag;

/**
 * @Authror: jcshen
 * @Date: 2020/12/29
 * @Version: 1.0
 * @Description:    其它可选API
 *      trigger——触发器
 *          定义window什么时候关闭，触发计算并输出结果
 *
 *      evictor——移除器
 *          定义移除某些数据的逻辑
 *      allowedLateness()——允许处理迟到的数据
 *      sideOutputLateData()——将迟到的数据放入侧输出流
 *      getSideOutput——获取侧输出流
 *
 */
public class Other_API {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        //从文件读取数据
//        DataStream<String> inputStream = env.readTextFile("F:\\workspace\\flink\\flink-11-java\\src\\main\\resources\\sensor.txt");
        //socket文本流
        DataStream<String> inputStream = env.socketTextStream("localhost", 7777);
        DataStream<SensorReading> dataStrem = inputStream.map(line -> {
            String[] fields = line.split(",");
            return new SensorReading(fields[0], new Long(fields[1]), new Double(fields[2]));
        });

        //3、其他API
        OutputTag<SensorReading> outputTag = new OutputTag<SensorReading>("late") {};
        SingleOutputStreamOperator<SensorReading> sumStream = dataStrem.keyBy("id")//
                .timeWindow(Time.seconds(15))
//                .trigger()
//                .evictor()
                .allowedLateness(Time.minutes(1))//延迟1分钟
                .sideOutputLateData(outputTag)//侧输出流
                .sum("temperature");
        //获取侧输出流
        sumStream.getSideOutput(outputTag).print("late");

        env.execute("other window job");
    }
}
