package com.org.window;

import com.org.bean.SensorReading;
import org.apache.flink.api.common.functions.AggregateFunction;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import scala.Tuple2;

/**
 * @Authror: jcshen
 * @Date: 2020/12/29
 * @Version: 1.0
 * @Description:
 */
public class Count_Window {
    /**
     *  1、nc -lk 7777
     *  2、启动程序
     *  3、输入sensor的数据
     */
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        //从文件读取数据
//        DataStream<String> inputStream = env.readTextFile("F:\\workspace\\flink\\flink-11-java\\src\\main\\resources\\sensor.txt");
        //socket文本流
        DataStream<String> inputStream = env.socketTextStream("localhost", 7777);
        DataStream<SensorReading> dataStrem = inputStream.map(line -> {
            String[] fields = line.split(",");
            return new SensorReading(fields[0], new Long(fields[1]), new Double(fields[2]));
        });
        //计数窗口测试
        dataStrem.keyBy("id")
                .countWindow(10,2)
                .aggregate(new AvgTemp());

        env.execute("count window job");
    }

    /**
     * 计算平均值
     */
    public static class AvgTemp implements AggregateFunction<SensorReading, Tuple2<Double,Integer>,Double> {

        @Override
        public Tuple2<Double, Integer> createAccumulator() {
            return new Tuple2<>(0.0,0);
        }

        @Override
        public Tuple2<Double, Integer> add(SensorReading value, Tuple2<Double, Integer> accumulator) {
            return new Tuple2<>(accumulator._1+value.getTemperature(),accumulator._2+1);
        }

        @Override
        public Double getResult(Tuple2<Double, Integer> accumulator) {
            return accumulator._1/accumulator._2;
        }

        @Override
        public Tuple2<Double, Integer> merge(Tuple2<Double, Integer> a, Tuple2<Double, Integer> b) {
            return new Tuple2<>(a._1+b._1,a._2+b._2);
        }
    }
}
