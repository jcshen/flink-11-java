package com.org.process;

import com.org.bean.SensorReading;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.util.Collector;

/**
 * @Authror: jcshen
 * @Date: 2021/1/1
 * @Version: 1.0
 * @Description:
 *          ProcessFunciton API（底层API）
 *              转换算子是无法访问事件的时间戳和Watermark的。
 *              基于此，DataStream API提供了一系列的Low-Level转换算子。可以访问时间戳、watermark以及注册定时事件。
 *              还可以输出特定的一些事件，如超时事件等。
 *              Process Function用来构建事件驱动的应用以及实现自定义的业务逻辑（使用之前的window函数和转换算子无法实现）。
 *              如FlinkSQL就是使用Process Function实现的。
 *          Flink提供了8个Process Function
 *              ProcessFunction
 *              KeyedProcessFunction
 *              CoProcessFunction
 *              ProcessJoinFunction
 *              BroadcastProcessFunction
 *              KeyedBroadcastProcessFunction
 *              ProcessWindowFunction
 *              ProcessAllWindowFunction
 */
public class Keyed_ProcessFunction {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        DataStream<String> inputStream = env.socketTextStream("localhost", 7777);
        DataStream<SensorReading> dataStream = inputStream.map(line -> {
            String[] fields = line.split(",");
            return new SensorReading(fields[0], new Long(fields[1]), new Double(fields[2]));
        });
        //测试KeyedProcessFunction，先分组然后自定义处理
        dataStream.keyBy("id")
                .process(new KeyedProcess())
                .print();

        env.execute("keyed process job");
    }

    /**
     * 实现自定义的处理函数
     */
    public static class KeyedProcess extends KeyedProcessFunction<Tuple,SensorReading,Integer> {
        ValueState<Long> tsTimerState;

        @Override
        public void open(Configuration parameters) throws Exception {
            tsTimerState = getRuntimeContext().getState(new ValueStateDescriptor<Long>("ts-timer",Long.class));
        }

        @Override
        public void processElement(SensorReading value, Context ctx, Collector<Integer> out) throws Exception {
            out.collect(value.getId().length());
            //context的作用
            ctx.timestamp();
            ctx.getCurrentKey();
//            ctx.output();//侧输出流
            ctx.timerService().currentProcessingTime();
            ctx.timerService().currentWatermark();

//            ctx.timerService().registerEventTimeTimer((value.getTimestamp()+10)*1000L);//注册事件时间定时器
//            ctx.timerService().deleteEventTimeTimer((value.getTimestamp()+10)*1000L);//删除定时器

            ctx.timerService().registerProcessingTimeTimer(ctx.timerService().currentProcessingTime()+1000L);//注册处理时间定时器
//            ctx.timerService().deleteProcessingTimeTimer(tsTimerState.value());//删除定时器

            tsTimerState.update(ctx.timerService().currentProcessingTime()+1000L);
        }

        //定义定时器
        @Override
        public void onTimer(long timestamp, OnTimerContext ctx, Collector<Integer> out) throws Exception {
            System.out.println(timestamp+"定时器触发");
            ctx.getCurrentKey();
//            ctx.output();
            ctx.timeDomain();
        }

        //清理状态
        @Override
        public void close() throws Exception {
            tsTimerState.clear();
        }
    }
}
