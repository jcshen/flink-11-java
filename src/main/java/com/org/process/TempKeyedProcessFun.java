package com.org.process;

import com.org.bean.SensorReading;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.util.Collector;

/**
 * @Authror: jcshen
 * @Date: 2021/1/1
 * @Version: 1.0
 * @Description: KeyedProcessFunction如何操作KeyedStream
 *      需求：监控传感器温度值，若温度值在10秒内（processint time）连续上升，则报警。
 *      使用ValueState状态变量来保存上次的温度值和定时器时间戳。
 */
public class TempKeyedProcessFun {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        DataStream<String> inputStream = env.socketTextStream("localhost", 7777);
        DataStream<SensorReading> dataStream = inputStream.map(line -> {
            String[] fields = line.split(",");
            return new SensorReading(fields[0], new Long(fields[1]), new Double(fields[2]));
        });

        dataStream.keyBy("id")
                .process(new TempConsIncreWarning(10))
                .print();

        env.execute("keyed process job");
    }

    /**
     * 实现自定义处理函数，检测一段时间内的温度连续上升，输出报警
     */
    public static class TempConsIncreWarning extends KeyedProcessFunction<Tuple,SensorReading,String> {
        //定义私有属性，当前统计的时间间隔
        private Integer interval;

        public TempConsIncreWarning(Integer interval) {
            this.interval=interval;
        }
        //定义状态，保存上一次的温度值，定时器时间戳
        private ValueState<Double> lastTempState;
        private ValueState<Long> tsTimerState;

        @Override
        public void open(Configuration parameters) throws Exception {
            //在生命周期中声明
            lastTempState = getRuntimeContext().getState(new ValueStateDescriptor<Double>("last-temp",Double.class,Double.MIN_VALUE));
            tsTimerState = getRuntimeContext().getState(new ValueStateDescriptor<Long>("ts-timer",Long.class));
        }

        @Override
        public void processElement(SensorReading value, Context ctx, Collector<String> out) throws Exception {
            //取出状态值
            Double lastTemp = lastTempState.value();
            Long timeTs = tsTimerState.value();
            //如果温度上升并且没有定时器，注册10秒后的定时器，开始等待
            if (value.getTemperature()>lastTemp&&timeTs==null){
                //计算出定时器时间戳
                long ts = ctx.timerService().currentProcessingTime() + interval * 1000L;
                //注册定时器
                ctx.timerService().registerProcessingTimeTimer(ts);
                //更新定时器状态
                tsTimerState.update(ts);
            }
            //如果温度下降，删除定时器
            else if (value.getTemperature()<lastTemp && timeTs!=null){
                //删除定时器
                ctx.timerService().deleteProcessingTimeTimer(timeTs);
                //清空定时器状态
                tsTimerState.clear();
            }

            //更新温度状态
            lastTempState.update(value.getTemperature());
        }

        @Override
        public void onTimer(long timestamp, OnTimerContext ctx, Collector<String> out) throws Exception {
            //定时器触发，输出报警信息
            out.collect("传感器"+ctx.getCurrentKey().getField(0)+"温度值连续"+interval+"秒上升");
            //清空定时器状态
            tsTimerState.clear();
        }

        @Override
        public void close() throws Exception {
            lastTempState.clear();
        }
    }
}
