package com.org.process;

import com.org.bean.SensorReading;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.util.Collector;
import org.apache.flink.util.OutputTag;

/**
 * @Authror: jcshen
 * @Date: 2021/1/1
 * @Version: 1.0
 * @Description:    侧输出流(SideOutPut)
 *      大部分DataStream API算子的输出是单一输出，除了split算子，将一条流分成多条流，这些流的数据类型也都相同。
 *      process function的sice outputs功能可以产生多条流，且这些流的数据类型可以不一样。
 *      一个side output可以定义为OutputTag[X]对象，X是输出流的数据类型。process function可以通过Context对象
 *      发射一个事件到一个或者多个side outputs。
 *
 *      示例：监控传感器温度值，将温度值低于30度的数据输出到side output。
 */
public class Side_Output {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        DataStream<String> inputStream = env.socketTextStream("localhost", 7777);
        DataStream<SensorReading> dataStream = inputStream.map(line -> {
            String[] fields = line.split(",");
            return new SensorReading(fields[0], new Long(fields[1]), new Double(fields[2]));
        });
        //定义一个OutputTag，用来表示侧输出流低温流
        OutputTag<SensorReading> lowTempTag = new OutputTag<SensorReading>("low-temp") {};

        //ProcessFunction，自定义侧输出流实现分流操作
        SingleOutputStreamOperator<SensorReading> highTempStream = dataStream.process(new ProcessFunction<SensorReading, SensorReading>() {
            @Override
            public void processElement(SensorReading value, Context ctx, Collector<SensorReading> out) throws Exception {
                //判断温度：大于30度，高温流输出到主流；小于低温流输出到到侧输出流
                if (value.getTemperature()>30){
                    out.collect(value);
                }else{
                    ctx.output(lowTempTag,value);
                }
            }
        });

        highTempStream.print("high-temp");
        highTempStream.getSideOutput(lowTempTag).print("low-temp");

        env.execute("side output job");
    }
}
