package com.org.wc;

/**
 * @Authror: jcshen
 * @Date: 2020/12/14
 * @Version: 1.0
 * @Description：
 */

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.util.Collector;

/**
 * 流处理
 *Flink keyby 数据倾斜问题处理
 *
 * 1.首先将key打散，我们加入将key转化为 key-随机数 ,保证数据散列
 * 2.对打散后的数据进行聚合统计，这时我们会得到数据比如 : (key1-12,1),(key1-13,19),(key1-1,20),(key2-123,11),(key2-123,10)
 * 3.将散列key还原成我们之前传入的key，这时我们的到数据是聚合统计后的结果，不是最初的原数据
 * 4.二次keyby进行结果统计，输出到addSink
 */
public class StreamWordCount {
    public static void main(String[] args) throws Exception {
        //创建流处理执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        //获取参数：parameter tool工具从程序启动参数中提取配置项
        ParameterTool paras = ParameterTool.fromArgs(args);
        String host = paras.get("host");
        int port = paras.getInt("port");

        //从文件中读取数据
        String inputPath ="F:\\workspace\\flink\\flink-11-java\\src\\main\\resources\\hello.txt";
//        DataStream<String> inputDS = env.readTextFile(inputPath);

        //从socket中读取数据
        DataStreamSource<String> inputDS = env.socketTextStream(host, port);
        //基于数据流进行转换计算
        DataStream<Tuple2<String, Integer>> resultSet = inputDS.flatMap(new FlatMapFunction<String, Tuple2<String, Integer>>() {
            public void flatMap(String value, Collector<Tuple2<String, Integer>> out) throws Exception {
                String[] words = value.split(" ");
                for (String word : words) {
                    out.collect(new Tuple2<String, Integer>(word, 1));
                }
            }
        })
                .keyBy(0)
                .sum(1);
        //打印输出
        resultSet.print();
        //执行任务
        env.execute("stream-wc");

    }
}
