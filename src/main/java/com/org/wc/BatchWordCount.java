package com.org.wc;

/**
 * @Authror: jcshen
 * @Date: 2020/12/14
 * @Version: 1.0
 * @Description：
 */

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.util.Collector;

/**
 * 批处理 word count
 */
public class BatchWordCount {
    public static void main(String[] args) throws Exception {
        //创建执行环境
        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
        //从文件中读取数据
        String inputPath ="F:\\workspace\\flink\\flink-11-java\\src\\main\\resources\\hello.txt";
        DataSet<String> inputDS = env.readTextFile(inputPath);
        //对数据进行处理
        DataSet<Tuple2<String, Integer>> resultSet = inputDS.flatMap(new FlatMapFunction<String, Tuple2<String, Integer>>() {
            //自定义类，实现FlatMapFunction接口
            public void flatMap(String value, Collector<Tuple2<String, Integer>> out) throws Exception {
                String[] words = value.split(" ");
                for (String word : words) {
                    //输出
                    out.collect(new Tuple2<String, Integer>(word, 1));
                }
            }
        })
                .groupBy(0)//按照第一个位置的word分组
                .sum(1);//将第二个位置上的数据求和
        //打印输出
        resultSet.print();
    }
}
