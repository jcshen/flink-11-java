package com.org.watermark;

import com.org.bean.SensorReading;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.AscendingTimestampExtractor;
import org.apache.flink.streaming.api.functions.timestamps.BoundedOutOfOrdernessTimestampExtractor;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.util.OutputTag;

/**
 * @Authror: jcshen
 * @Date: 2020/12/30
 * @Version: 1.0
 * @Description:
 *      乱序数据的影响
 *          当Flink以Event Time模式处理数据流时，它会根据数据里的时间戳来处理基于时间的算子；
 *          由于网络分布式等原因，会导致乱序数据的产生；
 *
 *      水位线（Watermark）
 *          怎样避免乱序数据带来的计算不正确？
 *          遇到一个时间戳达到了窗口关闭时间，不应该立刻触发窗口计算，而是等待一段时间，等迟到的数据来了再关闭窗口。
 *
 *          Watermark是一种衡量Event Time进展的机制，可以设定延迟触发；
 *          Watermark是用于处理乱序事件的，而正确的处理乱序事件，通常用Watermark机制结合window来实现；
 *          数据流中的Watermark用于表示timestamp小于Watermark的数据，都以到达，因此，window的执行也是由Watermark触发的；
 *          Watermark用来让程序自己平衡延迟和结果正确性
 *
 *     Watermark的特点
 *          Watermark是一条特殊的数据记录；
 *          Watermark必须单调递增，以确保事件的任务时间时钟在向前推进，而不是后退；
 *          Watermark与数据的时间戳相关；
 *
 *    Watermark的传递
 *          上游watermark广播到下游；取最小的时间戳
 *
 *   Watermark的设定
 *          在Flink中，Watermark由应用程序开发人员生成，通常需要对相应的领域有一定的了解；
 *          如果waermark设置的延迟太久，收到结果的速度可能就会很慢，解决办法是在watermark到达之前输出一个近似值；
 *          而如果watermark到达太早，则可能收到错误结果，不过Flink处理迟到数据的机制可以解决这个问题；
 *
 *
 *
 */
public class Event_TimeWindow {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);//设置全局时间语义
        //读取数据；分配时间戳和watermark
        DataStream<String> inputStream = env.socketTextStream("localhost", 7777);
        DataStream<SensorReading> dataStream = inputStream.map(line -> {
            String[] fields = line.split(",");
            return new SensorReading(fields[0], new Long(fields[1]), new Double(fields[2]));
        })
                //一种：升序数据设置事件时间和watermark，单调递增时间戳提取器
//                .assignTimestampsAndWatermarks(new AscendingTimestampExtractor<SensorReading>() {
//                    @Override
//                    public long extractAscendingTimestamp(SensorReading element) {
//                        return element.getTimestamp()*1000L;
//                    }
//                })
                //二种：乱序数据设置时间戳和watermark，有界乱序时间戳提取器，周期性生产watermark
                .assignTimestampsAndWatermarks(new BoundedOutOfOrdernessTimestampExtractor<SensorReading>(Time.seconds(2)) {//延迟2秒
                    @Override
                    public long extractTimestamp(SensorReading element) {
                        //提取时间戳
                        return element.getTimestamp()*1000L;
                    }
                });
        //定义侧输出流标签
        OutputTag<SensorReading> outputTag = new OutputTag<SensorReading>("late") {};

        //基于事件时间的开窗聚合，统计15秒内温度的最小值
        SingleOutputStreamOperator<SensorReading> minTempStream = dataStream.keyBy("id")
                .timeWindow(Time.seconds(15))
                .allowedLateness(Time.minutes(1))//1分钟迟到延时，每到15秒会输出，1分钟内只要来了就会更新一次
                .sideOutputLateData(outputTag)//最后乱序的就输出到侧输流
                .minBy("temperature");

        minTempStream.print("minTemp");
        minTempStream.getSideOutput(outputTag).print("late");

        env.execute("event time job");

    }
}
