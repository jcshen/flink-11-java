package com.org.function;

import com.org.bean.SensorReading;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.table.functions.TableFunction;
import org.apache.flink.types.Row;

/**
 * @Authror: jcshen
 * @Date: 2021/1/8
 * @Version: 1.0
 * @Description:    表函数（Table Functions）
 *      用户定义的表函数，也可以将0、1或多个标量函数值作为输入参数，与标量函数不同的是，
 *      它可以返回任意数量的行作为输出，而不是单个值；
 *      为了定义个表函数，必须扩展org.apache.flink.table.functions中的基类TableFunction并实现(一个或多个)求值方法；
 *      表函数的行为由其求值方法决定，求值方法必须是public的，并命名为eval；
 */
public class UDF_TableFunction {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        DataStream<String> intputStream = env.readTextFile("F:\\workspace\\flink\\flink-11-java\\src\\main\\resources\\sensor.txt");
        DataStream<SensorReading> dataStream = intputStream.map(line -> {
            String[] fields = line.split(",");
            return new SensorReading(fields[0], new Long(fields[1]), new Double(fields[2]));
        });
        //将流转换为表
        Table sensorTable = tableEnv.fromDataStream(dataStream, "id,timestamp as ts,temperature as temp");
        //自定义表函数，实现将id拆分，并输出（word,length）
        //Table API
        Split split = new Split("_");
        //在环境中注册UDF
        tableEnv.registerFunction("split",split);
        Table resultTable = sensorTable.joinLateral("split(id) as (word,length)")
                .select("id,ts,word,length");
        //SQL
        tableEnv.createTemporaryView("sensor",sensorTable);
        Table resultSqlTable = tableEnv.sqlQuery("select id,ts,word,length " +
                " from sensor,lateral table(split(id)) as splitid(word,length)");
        //打印
        tableEnv.toAppendStream(resultTable, Row.class).print("table");
        tableEnv.toAppendStream(resultSqlTable,Row.class).print("sql");

        env.execute("table function job");
    }
    //实现自定义TableFunction
    public static class Split extends TableFunction<Tuple2<String,Integer>>{
        private String separator;
        public Split(String separator){
            this.separator=separator;
        }
        //必须实现一个eval方法，没有返回值
        public void eval(String str){
            for(String s:str.split(separator)){
                collect(new Tuple2<>(s,s.length()));
            }
        }
    }

}
