package com.org.function;

import com.org.bean.SensorReading;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.table.functions.AggregateFunction;
import org.apache.flink.types.Row;

/**
 * @Authror: jcshen
 * @Date: 2021/1/12
 * @Version: 1.0
 * @Description:    聚合函数（Aggregate Functions）
 *      聚合函数
 *          用户自定义聚合函数（User-Defined Aggregate Functions，UDAGGs）
 *          可以把一个表中的数据，聚合成一个标量值；
 *          用户定义的聚合函数，是通过继承AggregateFunction抽象类实现的；
 *     AggregationFunction要求必须实现的方法：
 *          ——createAccumulator()
 *          ——accumulate()
 *          ——getValue()
 *    AggregateFunction的工作原理如下：
 *          ——首先，它需要一个累加器（Accumulator），用来保存聚合中间结果的数据结构，
 *          可以通过调用createAccumulator()方法创建空累加器；
 *          ——随后，对每个输入行调用函数的accumulate()方法来更新累加器；
 *          ——处理完所有行后，将调用函数的getValue()方法来计算并返回最终结果；
 */
public class UDF_AggregateFunction {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        DataStream<String> intputStream = env.readTextFile("F:\\workspace\\flink\\flink-11-java\\src\\main\\resources\\sensor.txt");
        DataStream<SensorReading> dataStream = intputStream.map(line -> {
            String[] fields = line.split(",");
            return new SensorReading(fields[0], new Long(fields[1]), new Double(fields[2]));
        });
        //将流转换为表
        Table sensorTable = tableEnv.fromDataStream(dataStream, "id,timestamp as ts,temperature as temp");
        //自定义聚合函数，求当前传感器的平均温度值
        //Table API
        AvgTemp avgTemp = new AvgTemp();
        //在环境中注册UDF
        tableEnv.registerFunction("avgTemp",avgTemp);
        Table resultTable = sensorTable.
                groupBy("id")
                .aggregate("avgTemp(temp) as avg_temp")
                .select("id,avg_temp");
        //SQL
        tableEnv.createTemporaryView("sensor",sensorTable);
        Table resultSqlTable = tableEnv.sqlQuery("select id,avgTemp(temp) as avg_temp" +
                " from sensor group by id");
        //打印，toRetractStream：有更新时使用
        tableEnv.toRetractStream(resultTable, Row.class).print("table");
        tableEnv.toRetractStream(resultSqlTable,Row.class).print("sql");

        env.execute("table function job");
    }

    /**
     * 实现自定义的AggregateFunction，AggregateFunction<Double
     * Tuple2<Double,Integer>>
     *     ——Double是返回的平均值；
     *     ——Tuple2<Double,Integer>，当前状态的类型是Tuple，Double是当前温度值，Integer是传感器的个数
     */
    public static class AvgTemp extends AggregateFunction<Double, Tuple2<Double,Integer>>{

        //获取结果
        @Override
        public Double getValue(Tuple2<Double, Integer> accumulator) {
            //计算平均值
            return accumulator.f0 / accumulator.f1;
        }

        //创建当前累加器
        @Override
        public Tuple2<Double, Integer> createAccumulator() {
            return new Tuple2<Double, Integer>(0.0,0);
        }

        /**
         * 必须实现一个accumulate方法，来数据之后更新状态
         * Tuple2<Double,Integer> acc——当前状态
         * Double temp——当前温度
         */
        public void accumulate(Tuple2<Double,Integer> acc,Double temp){
            acc.f0 += temp;
            acc.f1 += 1;
        }
    }
}
