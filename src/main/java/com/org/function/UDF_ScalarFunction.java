package com.org.function;

import com.org.bean.SensorReading;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.table.functions.ScalarFunction;
import org.apache.flink.types.Row;

/**
 * @Authror: jcshen
 * @Date: 2021/1/8
 * @Version: 1.0
 * @Description:    用户自定义函数（UDF）
 *      用户自定义函数（User-defined Functions,UDF）是一个重要的特性，它们显著的扩展了查询的表达能力；
 *      在大多数情况下，用户自定义的函数必须先注册，然后才能在查询中使用；
 *      函数通过调用registerFunction()方法在TabeEnvironment中注册。当用户定义的函数被注册时，
 *      被插入到TableEnvironment的函数目录中，这样Table API或SQL解析就可以识别并正确地解释；
 *
 *      标量函数（Scalar Functions）
 *          用户定义的标量函数，可以将0、1或多个标量值，映射到新的标量值
 *          为了定义标量函数，必须在org.apache.flink.table.functions中扩展基类Scalar Function，并
 *          实现（一个或多个）求值（eval）方法；
 *          标量函数的行为由求值方法决定，求值方法必须公开声明并命名为eval；
 *
 */
public class UDF_ScalarFunction {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        DataStream<String> intputStream = env.readTextFile("F:\\workspace\\flink\\flink-11-java\\src\\main\\resources\\sensor.txt");
        DataStream<SensorReading> dataStream = intputStream.map(line -> {
            String[] fields = line.split(",");
            return new SensorReading(fields[0], new Long(fields[1]), new Double(fields[2]));
        });
        //将流转换为表
        Table sensorTable = tableEnv.fromDataStream(dataStream, "id,timestamp as ts,temperature as temp");
        //自定义标量函数，实现求id的hash值
        HashCode hashCode = new HashCode(23);
        //需要在环境中注册UDF
        tableEnv.registerFunction("hashCode",hashCode);
        //Table API
        Table resultTable = sensorTable.select("id,ts,temp,hashCode(id)");
        //SQL
        tableEnv.createTemporaryView("sensor",sensorTable);
        Table resultSqlTable = tableEnv.sqlQuery("select id,ts,temp,hashCode(id) from sensor");
        //打印
        tableEnv.toAppendStream(resultTable, Row.class).print("table");
        tableEnv.toAppendStream(resultSqlTable,Row.class).print("sql");

        env.execute("scalar function job");
    }
    //实现自定义的ScalarFunction
    public static class HashCode extends ScalarFunction{
        private int factor = 13;
        public HashCode(int factor){
            this.factor=factor;
        }

        public int eval(String str){
            return str.hashCode()*factor;
        }
    }
}
