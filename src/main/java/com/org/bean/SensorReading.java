package com.org.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @Authror: jcshen
 * @Date: 2020/12/23
 * @Version: 1.0
 * @Description:传感器温度
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SensorReading {
    //属性：id,时间戳，温度值
    private String id;
    private Long timestamp;
    private Double temperature;

}
