package com.org.table;

import com.org.bean.SensorReading;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.DataTypes;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.table.descriptors.Csv;
import org.apache.flink.table.descriptors.Kafka;
import org.apache.flink.table.descriptors.Schema;
import org.apache.flink.types.Row;

/**
 * @Authror: jcshen
 * @Date: 2021/1/3
 * @Version: 1.0
 * @Description:    时间特性（Time Attributes）
 *         基于时间的操作（如Table API和SQL中窗口操作），需要定义相关的时间语义和时间数据来源的信息；
 *         Table可以提供一个逻辑上的时间字段，用于在表处理程序中，指示时间和访问相应的时间戳；
 *         时间属性，可以是每个表schema的以部。一旦定义了时间属性，它就可以作为一个字段引用，并且可以在基于时间的操作中使用；
 *         时间属性的行为类似于常规时间戳，可以访问，并且进行计算；
 *
 */
public class ProcessTimeAndWindow {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);
        //读取数据
        DataStream<String> inputStream = env.readTextFile("F:\\workspace\\flink\\flink-11-java\\src\\main\\resources\\sensor.txt");
        DataStream<SensorReading> dataStream = inputStream.map(line -> {
            String[] fields = line.split(",");
            return new SensorReading(fields[0], new Long(fields[1]), new Double(fields[2]));
        });
        //将流转换成表，定义时间特性；定义处理时间语义
        Table dataTable = tableEnv.fromDataStream(dataStream, "id,timestamp as ts,temperature as temp,pt.proctime");
        //连接Kafka时，定义Table Schema时指定处理时间语义
        tableEnv.connect(new Kafka()
                .version("0.11")
                .topic("sensor")
                .property("zookeeper.connect","localhost:2181")
                .property("bootstrap.servers","loacalhost:9092"))
                .withFormat(new Csv())
                .withSchema(new Schema()
                        .field("id", DataTypes.STRING())
                        .field("ts",DataTypes.BIGINT())
                        .field("temp",DataTypes.DOUBLE())
                        .field("pt",DataTypes.TIMESTAMP(3))
                        .proctime()
                )
                .createTemporaryTable("inputTable");
        //在创建表的DDL中定义
        String sinkDDL="create table dataTable(" +
                "id varchar(20) not null," +
                "ts bigint," +
                "temp double," +
                "pt AS PROCTIME()" +
                ") with (" +
                "'connector.type' = 'filesystem'," +
                "'connector.path' = '/sensor.txt'," +
                "'format.type' = 'csv')";
        tableEnv.sqlUpdate(sinkDDL);

        tableEnv.toAppendStream(dataTable, Row.class).print();

        env.execute("time and window job");
    }
}
