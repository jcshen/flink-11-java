package com.org.table;

import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.DataTypes;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.table.descriptors.*;

/**
 * @Authror: jcshen
 * @Date: 2021/1/2
 * @Version: 1.0
 * @Description:    更新模式
 *      对于流式查询，需要声明如何在表和外部连接器之间执行转换；
 *      与外部系统交换的消息类型，由更新模式（Update Mode）指定；
 *      追加（Append）模式
 *          表只做插入操作，和外部连接器只交换插入（insert）消息
 *      撤回（Retract）模式
 *          表和外部连接器交换添加（Add）和撤回（Retract）消息；
 *          插入操作（insert）编码为Add消息，删除（Delete）编码为Retract消息，
 *          更新（Update）编码为上一条的Retract和下一条的Add消息；
 *      更新插入（Upsert）模式
 *          更新和插入都被编码为Upsert消息，删除编码为Delete消息；
 */
public class TableUpsertEs {
    //输出到ES：可以创建Table来描述ES中得到数据，作为输出的TableSink
    public static void main(String[] args) throws Exception {
        //输出到ES：可以创建Table来描述ES中得到数据，作为输出的TableSink
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);
        //连接Kafka，读取数据
        tableEnv.connect(new Kafka()
                .version("0.11")
                .topic("sensor")
                .property("zookeeper.connect","localhost:2181")
                .property("bootstrap.servers","loacalhost:9092"))
                .withFormat(new Csv())
                .withSchema(new Schema()
                        .field("id", DataTypes.STRING())
                        .field("ts",DataTypes.BIGINT())
                        .field("temp",DataTypes.DOUBLE())
                )
                .createTemporaryTable("inputTable");

        //读取当前的表
        Table inputTable = tableEnv.from("inputTable");
        //聚合统计
        Table aggTable = inputTable.groupBy("id")
                .select("id,id.count as cnt,temp.avg as t_avg");

        tableEnv.connect(new Elasticsearch()
        .version("7")
        .host("localhost",9200,"http")
                .index("sensor")
                .documentType("temp")
        )
        .inUpsertMode()
                .withFormat(new Json())
                .withSchema(new Schema()
                .field("id",DataTypes.STRING())
                .field("count",DataTypes.BIGINT())
                )
                .createTemporaryTable("esoutputTable");

        aggTable.executeInsert("esoutputTable");

        env.execute("es table job");
    }
}
