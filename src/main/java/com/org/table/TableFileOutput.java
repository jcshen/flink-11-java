package com.org.table;

import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.DataTypes;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.table.descriptors.Csv;
import org.apache.flink.table.descriptors.FileSystem;
import org.apache.flink.table.descriptors.Schema;

/**
 * @Authror: jcshen
 * @Date: 2021/1/2
 * @Version: 1.0
 * @Description:    表（Table）
 *      TableEnvironment可注册目录Catlog，并可以基于Catalog注册表
 *      表（Table）是由一个"标识符"（identifier）来指定的，由3部分组成：Catalog名、数据库（database）名和对象名
 *      表可以是常规的，也可以是虚拟的（视图，View）
 *      常规表（Table）一般可用来描述外部数据，比如文件、数据库表或消息队列的数据，也可以直接从DataStream转换而来
 *      视图（View）可以从现有的表中创建，通常是table API或者SQL查询的一个结果集
 *
 *      创建表
 *          TableEnvironment调用.connect()方法，连接外部系统，并调用.createTemporaryTable()方法，在Catalog中注册表
 *          tableEnv
 *              .connect(...)//定义表的数据来源，和外部系统建立连接
 *              .withFormat(...)//定义数据格式化方法
 *              .withSchema(...)//定义表结构
 *              .createTemporaryTable("myTable")//创建临时表
 *
 */
public class TableFileOutput {
    /**
     * TODO
     *报错
     * No operators defined in streaming topology. Cannot execute
     */
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        env.setParallelism(1);
//        EnvironmentSettings bsSettings = EnvironmentSettings.newInstance().useBlinkPlanner().inStreamingMode().build();

        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        String filePath="F:\\workspace\\flink\\flink-11-java\\src\\main\\resources\\sensor.txt";
        tableEnv.connect(new FileSystem().path(filePath))
                .withFormat(new Csv())
                .withSchema(new Schema()
                .field("id", DataTypes.STRING())
                .field("ts",DataTypes.BIGINT())
                .field("temp",DataTypes.DOUBLE()))
                .createTemporaryTable("inputTable");

        Table inputTable = tableEnv.from("inputTable");

        Table resultTable = inputTable.select("id,temp")
                .filter("id='sensor_6'");
//        Table aggTable = inputTable.groupBy("id")
//                .select("id,id.count as cnt,temp.avg as t_avg");

//        tableEnv.sqlQuery("select id,temp from inputTable where id='sensor_6'");
//        Table sqlAggTable = tableEnv.sqlQuery("select id,count(id) as cnt,avg(temp) t_avg from inputTable group by id");
        //4、输出到文件
        //连接外部文件注册输出表
        String outputPath="F:\\workspace\\flink\\flink-11-java\\src\\main\\resources\\output.txt";
        tableEnv.connect(new FileSystem().path(outputPath))
                .withFormat(new Csv())
                .withSchema(new Schema()
                .field("id",DataTypes.STRING())
                .field("temp",DataTypes.DOUBLE()))
                .createTemporaryTable("outputTable");
        //写入文件
        resultTable.executeInsert("outputTable");
//        tableEnv.toRetractStream(resultTable,Row.class).print();

        env.execute("file output job");
    }
}
