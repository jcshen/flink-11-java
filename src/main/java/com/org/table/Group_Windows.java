package com.org.table;

import com.org.bean.SensorReading;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.timestamps.BoundedOutOfOrdernessTimestampExtractor;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.Tumble;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.types.Row;

/**
 * @Authror: jcshen
 * @Date: 2021/1/4
 * @Version: 1.0
 * @Description:    窗口
 *      时间语义，要配合窗口操作才能发挥作用；
 *      在Table API和SQL中，主要有两种窗口：Group Windows和 Over Windows
 *          Group Windows（分组窗口）
 *              根据时间或行计数间隔，将行聚合到有限的组（Group）中，并对每个组的数据执行一次聚合函数；
 *              Group Windows是使用window子句定义的，并且必须由as子句指定一个别名；
 *              为了按窗口对表进行分组，窗口的别名必须在group by子句中，像常规的分组字段一样引用；
 *
 *              滚动窗口（Tumbling windows）：滚动窗口要用Tumble类来定义：
 *                  //Tumbling Event-time window
 *                  .window(Tumble.over("10.minutes").on("rowtime").as("w"))
 *                  //Tumbling Processing-time window
 *                  .window(Tumble.over("10.minutes").on("proctime").as("w"))
 *                  //Tumbling Row-count Windiw
 *                  .window(Tumble.over("10.rows").on("proctime").as("w"))
 *             滑动窗口（Sliding windows）：滑动窗口要用Slide类来定义
 *                  //Sliding Event-time window
 *                  .window(Slide.over("10.minutes").every("5.minutes").on("rowtime").as("w"))
 *                  //Sliding Processing-time window
 *                  .window(Slide.over("10.minutes").every("5.mintes").on("proctime").as("w"))
 *                  //Sliding Row-count window
 *                  .window(Slide.over("10.rows").every("5.rows").on("proctime").as(""w))
 *            会话窗口（Session windows）：会话窗口要用session类来定义
 *                  //session Event-time window
 *                  .window(Session.withGap("10.minutes").on("rowtime").as("w"))
 *                  //session processing-time window
 *                  window(Session.withGap("10.minutes").on("proctime").as("w"))
 *     SQL中的Group windows
 *          Group Windows定义在SQL查询的Group by 子句中
 *              TUMBLE(time_attr,interval)
 *          定义一个滚动窗口，第一个参数是时间字段，第二个参数是窗口长度
 *              HOP(time_attr,interval,interval)
 *          定义一个滑动窗口，第一个参数是时间字段，第二个参数是窗口滑动步长，第三个是窗口长度
 *              SESSION(time_attr,interval)
 *          定义个会话窗口，第一个参数是时间字段，第二个参数是窗口间隔
 *
 *
 */
public class Group_Windows {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);

        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        DataStream<String> inputStream = env.readTextFile("F:\\workspace\\flink\\flink-11-java\\src\\main\\resources\\sensor.txt");
        DataStream<SensorReading> dataStream = inputStream.map(line -> {
            String[] fields = line.split(",");
            return new SensorReading(fields[0], new Long(fields[1]), new Double(fields[2]));
        })
                .assignTimestampsAndWatermarks(new BoundedOutOfOrdernessTimestampExtractor<SensorReading>(Time.seconds(2)) {
                    @Override
                    public long extractTimestamp(SensorReading element) {
                        return element.getTimestamp()*1000L;
                    }
                });

        Table dataTable = tableEnv.fromDataStream(dataStream, "id,timestamp as ts,temperature as temp,rt.rowtime");
        tableEnv.createTemporaryView("sensor",dataTable);
        //窗口操作
        //Group window
        //table api
        Table resultTable = dataTable.window(Tumble.over("10.seconds").on("rt").as("tw"))
                .groupBy("id,tw")
                .select("id,id.count,temp.avg,tw.end");
        //SQL
        Table sqlTable = tableEnv.sqlQuery("select id,count(id) as cnt,avg(temp) as t_avg,tumble_start(rt,interval '10' second),tumble_end(rt,interval '10' second)" +
                " from sensor group by id,tumble(rt,interval '10' second)");

        tableEnv.toAppendStream(resultTable, Row.class).print("table");
        tableEnv.toRetractStream(sqlTable,Row.class).print("sql");

        env.execute("group window job");
    }
}
