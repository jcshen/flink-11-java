package com.org.table;

import com.org.bean.SensorReading;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.timestamps.BoundedOutOfOrdernessTimestampExtractor;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.table.api.DataTypes;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.table.descriptors.Csv;
import org.apache.flink.table.descriptors.Kafka;
import org.apache.flink.table.descriptors.Rowtime;
import org.apache.flink.table.descriptors.Schema;
import org.apache.flink.types.Row;

/**
 * @Authror: jcshen
 * @Date: 2021/1/4
 * @Version: 1.0
 * @Description:    定义事件时间（Event Time）
 *      事件时间语义，允许表处理程序根据每个记录中包含的时间生成结果。这样即使在又乱序事件或者延迟事件时，也可获得正确的结果。
 *      为了处理无序事件，并区分流中的准时和延迟事件，Flink需要从事件数据中提取时间戳，并用来推进事件时间的进展。
 *      定义事件时间，同样有三种方法：
 *          由DataStream转换成表时指定；
 *          定义Table Schema时指定；
 *          在创建表的DDL中定义；
 *
 *
 */
public class EventTimeAndWindow {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);

        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        DataStream<String> inputStream = env.readTextFile("F:\\workspace\\flink\\flink-11-java\\src\\main\\resources\\sensor.txt");
        DataStream<SensorReading> dataStream = inputStream.map(line -> {
            String[] fields = line.split(",");
            return new SensorReading(fields[0], new Long(fields[1]), new Double(fields[2]));
        })
                .assignTimestampsAndWatermarks(new BoundedOutOfOrdernessTimestampExtractor<SensorReading>(Time.seconds(2)) {
                    @Override
                    public long extractTimestamp(SensorReading element) {
                        return element.getTimestamp()*1000L;
                    }
                });

//        Table table = tableEnv.fromDataStream(dataStream, "id,timestamp.rowtime as ts,temperature as temp");
        Table table = tableEnv.fromDataStream(dataStream, "id,timestamp as ts,temperature as temp,rt.rowtime");
        //定义Table Schema时指定
        tableEnv.connect(new Kafka()
                .version("0.11")
                .topic("sensor")
                .property("zookeeper.connect","localhost:2181")
                .property("bootstrap.servers","loacalhost:9092"))
                .withFormat(new Csv())
                .withSchema(new Schema()
                        .field("id", DataTypes.STRING())
                        .field("ts",DataTypes.BIGINT())
                        .rowtime(new Rowtime()
                        .timestampsFromField("timestamp")//从字段中提取时间戳
                        .watermarksPeriodicBounded(1000)//watermark延迟1秒
                        )
                        .field("temp",DataTypes.DOUBLE())
                )
                .createTemporaryTable("inputTable");
        //在创建表的DDL中定义
        String sinkDDL="create table dataTable(" +
                "id varchar(20) not null," +
                "ts bigint," +
                "temp double," +
                "rt AS TO_TIMESTAMP(FROM_UNIXTIME(ts))," +
                "watermark for rt as rt - interval '1' second" +
                ") with (" +
                "'connector.type' = 'filesystem'," +
                "'connector.path' = '/sensor.txt'," +
                "'format.type' = 'csv')";
        tableEnv.sqlQuery(sinkDDL);

        table.printSchema();
        tableEnv.toAppendStream(table, Row.class).print();

        env.execute("event time job");
    }
}
