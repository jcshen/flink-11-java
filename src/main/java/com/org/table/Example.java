package com.org.table;

import com.org.bean.SensorReading;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.types.Row;

/**
 * @Authror: jcshen
 * @Date: 2021/1/2
 * @Version: 1.0
 * @Description:    基本程序结构
 *          Table API和SQL的程序结构，与流式处理的程序结构十分类似
 *          StreamTableEnvironment tableEnv=... //创建表执行环境
 *          //创建一张表，用于读取数据
 *          tableEnv.connect(...).createTemporaryTable("inputTable")
 *          //注册一张表，用于把计算结构输出
 *          tableEnv.connet(...)createTemporaryTable("outputTable")
 *          //通过table API查询算子，得到一张结果表
 *          Table result=tableEnv.from("inputTable").select(...)
 *          //通过SQL查询语句，得到一张结果表
 *          Table sqlResult=tableEnv.sqlQuery("select ... from inputTable ...")
 *          //将结果表写入输出表
 *          result.insertInto(outputTable)
 *
 */
public class Example {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        DataStreamSource<String> inputStream = env.readTextFile("F:\\workspace\\flink\\flink-11-java\\src\\main\\resources\\sensor.txt");
        DataStream<SensorReading> dataStream = inputStream.map(line -> {
            String[] fields = line.split(",");
            return new SensorReading(fields[0], new Long(fields[1]), new Double(fields[2]));
        });
        //创建表环境
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);
        //基于流创建一张表
        Table dataTable = tableEnv.fromDataStream(dataStream);
        //调用table API进行转换操作
        Table resultTable = dataTable.select("id,temperature")
                .where("id='sensor_1'");
        //执行SQL
        tableEnv.createTemporaryView("sensor",dataTable);
        String sql = "select id,temperature from sensor where id='sensor_1'";
        Table resultSql = tableEnv.sqlQuery(sql);
        //转换为DataStream
        tableEnv.toAppendStream(resultTable, Row.class).print("table");
        tableEnv.toAppendStream(resultSql,Row.class).print("sql");

        env.execute("tabl job");
    }
}
