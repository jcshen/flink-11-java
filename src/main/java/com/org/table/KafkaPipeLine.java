package com.org.table;

import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.DataTypes;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.table.descriptors.Csv;
import org.apache.flink.table.descriptors.Kafka;
import org.apache.flink.table.descriptors.Schema;

/**
 * @Authror: jcshen
 * @Date: 2021/1/2
 * @Version: 1.0
 * @Description:
 *  1、启动zk、Kafka
 *  2、启动程序
 *  3、bin/kafka-console-producer.sh --broker-list localhost:9092 --topic sensor
 *  4、bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic sensor_sink
 *  5、在Kafka的producer中输入数据：
 *  6、查看Kafka的consumer是否有数据
 */
public class KafkaPipeLine {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);
        //连接Kafka，读取数据
        tableEnv.connect(new Kafka()
        .version("0.11")
        .topic("sensor")
        .property("zookeeper.connect","localhost:2181")
        .property("bootstrap.servers","loacalhost:9092"))
                .withFormat(new Csv())
                .withSchema(new Schema()
                .field("id", DataTypes.STRING())
                .field("ts",DataTypes.BIGINT())
                .field("temp",DataTypes.DOUBLE())
                )
                .createTemporaryTable("inputTable");

        //读取当前的表
        Table inputTable = tableEnv.from("inputTable");
        //查询转换
        Table resultTable = inputTable.select("id,temp")
                .filter("id='sensor_6'");
        //聚合统计
        Table aggTable = inputTable.groupBy("id")
                .select("id,id.count as cnt,temp.avg as t_avg");

        //建立Kafka连接，输出到不同的topic
        tableEnv.connect(new Kafka()
        .version("0.11")
        .topic("sensor_sink")
        .property("zookeeper.connect","localhost:2181")
        .property("bootstrap.servers","loacalhost:9092"))
        .withFormat(new Csv())
        .withSchema(new Schema()
                        .field("id", DataTypes.STRING())
//                        .field("ts",DataTypes.BIGINT())
                        .field("temp",DataTypes.DOUBLE())
        )
        .createTemporaryTable("outputTable");
        //插入数据
        resultTable.executeInsert("outputTable");

        env.execute("kafka pipeline job");
    }
}
