package com.org.table;

/**
 * @Authror: jcshen
 * @Date: 2021/1/3
 * @Version: 1.0
 * @Description:    动态表（Dynamic Tables） 和持续查询（Continuous Query）
 *      动态表
 *          动态表是Flink对数据流的Table API和SQL支持的核心概念；
 *          与表示批处理数据的静态表不同，动态表是随时间变化的；
 *
 *      持续查询
 *          动态表可像静态的批处理一样进行查询，查询一个动态表会产生持续查询（Continuous Query）；
 *          连续查询永远不会终止，并会生成另一个动态表；
 *          查询会不断更新其动态结果表，以反映其动态输入表上的更改
 *
 *     流式表查询的处理过程：
 *          1、流被转换为动态表
 *          2、对动态表计算连续查询，生成新的动态表
 *          3、生成的动态表被转换回流
 *
 *    将动态表转换成DataStream
 *          与常规的数据库一样，动态表可以通过插入（insert）、更新（Update）和删除（Delete），进行持续的修改
 *          将动态表转换为流或将其写入外部系统时，需要对这些更改进行编码
 *
 *          仅追加流（Append-only）：仅通过插入（insert）来修改的动态表，可直接转换为仅追加流
 *          撤回流（Retract）:撤回流是包含两类消息的流：添加（Add）消息和撤回（Retract）消息
 *          Upsert（更新插入）流：Upsert流和包含两种类型的消息：Upsert消息和删除（Delete）消息
 *
 *
 *
 */
public class DynamicTable_ContinuousQuery {
}
