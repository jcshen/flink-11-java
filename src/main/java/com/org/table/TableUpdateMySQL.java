package com.org.table;

import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.DataTypes;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.table.descriptors.Csv;
import org.apache.flink.table.descriptors.Kafka;
import org.apache.flink.table.descriptors.Schema;

/**
 * @Authror: jcshen
 * @Date: 2021/1/2
 * @Version: 1.0
 * @Description:    输出到MySQL
 *      可以创建table来描述MySQL中的数据，作为输入和输出
 */
public class TableUpdateMySQL {
    public static void main(String[] args) throws Exception {
        //输出到ES：可以创建Table来描述ES中得到数据，作为输出的TableSink
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        String sinkDDL="create table jdbcOutputTable(" +
                "id varchar(20) not null," +
                "cnt bigint not null" +
                ") with (" +
                "'connector.type' = 'jdbc'," +
                "'connector.url' = 'jdbc:mysql://localhost:3306/flink'," +
                "'connector.table' = 'sensor_count'," +
                "'connector.driver' = 'com.mysql.jdbc.Driver'," +
                "'connector.username' = 'root'," +
                "'connector.password' = '123456')";
        //执行DDL创建表
        tableEnv.sqlUpdate(sinkDDL);

        //连接Kafka，读取数据
        tableEnv.connect(new Kafka()
                .version("0.11")
                .topic("sensor")
                .property("zookeeper.connect","localhost:2181")
                .property("bootstrap.servers","loacalhost:9092"))
                .withFormat(new Csv())
                .withSchema(new Schema()
                        .field("id", DataTypes.STRING())
                        .field("ts",DataTypes.BIGINT())
                        .field("temp",DataTypes.DOUBLE())
                )
                .createTemporaryTable("inputTable");

        //读取当前的表
        Table inputTable = tableEnv.from("inputTable");
        //聚合统计
        Table aggTable = inputTable.groupBy("id")
                .select("id,id.count as cnt,temp.avg as t_avg");

        aggTable.executeInsert("jdbcOutputTable");

        env.execute("table to mysql job");
    }
}
