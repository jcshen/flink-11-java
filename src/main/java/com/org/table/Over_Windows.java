package com.org.table;

import com.org.bean.SensorReading;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.timestamps.BoundedOutOfOrdernessTimestampExtractor;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.table.api.Over;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.types.Row;

/**
 * @Authror: jcshen
 * @Date: 2021/1/6
 * @Version: 1.0
 * @Description:    Over Windows：针对每个输入行，计算相邻行范围内的聚合
 *         Over Windwos聚合是指标准SQL中已有的（over）子句，可以查询的SELECT子句中定义；
 *         Over Window聚合，会针对每个输入行，计算相邻行范围内的聚合；
 *         Over Window使用window（w:overwindows*）子句定义，并在select()方法中通过别名来引用
 *         Table table = input.window([w:Overwindow] as 'w').select('a,b.sum over w,c.min over w');
 *
 *         Table API提供了Over类，来配置Over窗口的属性
 *              无界Over Windows
 *                  可以在事件时间和处理时间，以及指定为时间间隔、或行计数的范围内，定义Over Windows；
 *                  无界的over window 是使用常量指定的；
 *                  //无界的事件时间over window
 *                  .window(Over.partitionBy("a").orderBy("rowtime").preceding(UNBOUNDED_RANGE).as("w"))
 *                  //无界的处理时间
 *                  .window(Over.partitionBy("a").orderBy("proctime").preceding(UNBOUNDED_RANGE).as("w"))
 *                  //无界的事件时间 Row-count over window
 *                  .window(Over.partitionBy("a").orderBy("rowtime").preceding(UNBOUNDED_ROW).as("w"))
 *                  //无界的处理时间Row-count over window
 *                  .window(Over.partitionBy("a").orderBy("proctime").preceding(UNBOUNDED_ROW).as("w"))
 *             有界over window是用间隔的大小指定的
 *                  //有界的事件时间over window
 *                  .widow(Over.partitionBy("a").orderBy("rowtime").preceding("1.minutes").as("w"))
 *                  //有界的处理时间over window
 *                  .window(Over.partitionBy("a")orderBy("protime").preceding("1.minutes").as("w"))
 *                  //有界的事件时间Row-count over window
 *                  .window(Over.partitionBy("a").orderBy("rowtime").preceding("10.rows").as("w"))
 *                  //有界的处理时间Row-count over window
 *                  .window(Over.partitionBy("a").orderBy("protime").preceding("10.rows").as("w"))
 *
 *      SQL中的Over Windows
 *            用Over做窗口聚合时，所有聚合必须在同一窗口上定义，也就是说必须是相同的分区、排序和范围；
 *           目前仅支持在当前行范围之前的窗口；
 *           ORDER BY必须在单一得到时间属性上指定
 *
 *           SELECT COUNT(amount) OVER(
 *                  PARTITION BY user
 *                  ORDER BY protime
 *                  ROWS BETWEEN 2 PRECEDING AND CURRENT ROW)
 *                  FROM orders
 *
 *
 */
public class Over_Windows {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);

        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        DataStream<String> inputStream = env.readTextFile("F:\\workspace\\flink\\flink-11-java\\src\\main\\resources\\sensor.txt");
        DataStream<SensorReading> dataStream = inputStream.map(line -> {
            String[] fields = line.split(",");
            return new SensorReading(fields[0], new Long(fields[1]), new Double(fields[2]));
        })
                .assignTimestampsAndWatermarks(new BoundedOutOfOrdernessTimestampExtractor<SensorReading>(Time.seconds(2)) {
                    @Override
                    public long extractTimestamp(SensorReading element) {
                        return element.getTimestamp()*1000L;
                    }
                });

        Table dataTable = tableEnv.fromDataStream(dataStream, "id,timestamp as ts,temperature as temp,rt.rowtime");
        tableEnv.createTemporaryView("sensor",dataTable);
        //Over window
        //Table API
        Table overResult = dataTable.window(Over.partitionBy("id").orderBy("rt").preceding("2.rows").as("ow"))
                .select("id,rt,id.count over ow,temp.avg over ow");
        //SQL
        Table overSqlResult = tableEnv.sqlQuery("select id,rt,count(id) over ow,avg(temp) over ow " +
                "from sensor " +
                "window ow as (partition by id order by rt rows between 2 preceding and current row)");
        //打印
        tableEnv.toAppendStream(overResult, Row.class).print("table");
        tableEnv.toRetractStream(overSqlResult,Row.class).print("sql");

        env.execute("over window job");
    }
}
