package com.org.source;

import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 * @Authror: jcshen
 * @Date: 2020/12/23
 * @Version: 1.0
 * @Description:读取文件中数据
 */
public class SourceFile {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        //从文件中读取数据
        DataStream<String> dataStream = env.readTextFile("F:\\workspace\\flink\\flink-11-java\\src\\main\\resources\\sensor.txt");
        //打印
        dataStream.print();
        //执行
        env.execute("file job");
    }
}
