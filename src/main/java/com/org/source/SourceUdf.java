package com.org.source;

import com.org.bean.SensorReading;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.source.SourceFunction;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * @Authror: jcshen
 * @Date: 2020/12/26
 * @Version: 1.0
 * @Description：自定义Source，模拟生产SensorReading数据
 */
public class SourceUdf {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        //从文件读取数据
        DataStream<SensorReading> dataStream =env.addSource(new SensorSource());
        //打印输出
        dataStream.print();
        env.execute("udf job");

    }

    /**
     * 实现自定义的SourceFunction
     * 模拟生成数据
     */
    public static class SensorSource implements SourceFunction<SensorReading> {
        //定义一个标识位，用来控制数据的产生
        private boolean running = true;

        public void run(SourceContext<SensorReading> ctx) throws Exception {
            //模拟数据
            //1、定义一个随机数生成器
            Random random = new Random();
            //2、设置10个传感器的初始温度
            Map<String,Double> map = new HashMap<String,Double>();
            for (int i = 0; i < 10; i++) {
                //高斯，正态分布
                map.put("sensor_"+(i+1),60 + random.nextGaussian()*20);
            }

            //循环处理数据
            while (running){
                //根据sensorId遍历
                for (String sensorId : map.keySet()) {
                    //在当前温度基础上随机波动
                    double newtemp = map.get(sensorId) + random.nextGaussian();
                    map.put(sensorId,newtemp);
                    //将数据输出
                    ctx.collect(new SensorReading(sensorId,System.currentTimeMillis(),newtemp));
                }
                //控制输出频率
                Thread.sleep(1000L);
            }

        }

        public void cancel() {
            running=false;
        }
    }
}
