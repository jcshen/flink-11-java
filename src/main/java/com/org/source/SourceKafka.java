package com.org.source;

import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer011;

import java.util.Properties;

/**
 * @Authror: jcshen
 * @Date: 2020/12/24
 * @Version: 1.0
 * @Description:Kafka中获取数据
 * 注意：
 * 1、启动Kafka生产者：bin/kafka-console-producer.sh --broker-list localhost:9092 --topic sensor
 * 2、启动程序
 * 3、Kafka生产者中输入数据
 */
public class SourceKafka {
    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers","localhost:9092");
        properties.setProperty("group.id","consumer-group");
        properties.setProperty("key.deserializer","org.apache.kafka.common.serialization.StringDeserializer");
        properties.setProperty("value.deserializer","org.apache.kafka.common.serialization.StringDeserializer");
        properties.setProperty("auto.offset.reset","latest");
        //从Kafka读取数据
        DataStream<String> dataStream =
                env.addSource(new FlinkKafkaConsumer011<String>("sensor", new SimpleStringSchema(),properties));
        //打印输出
        dataStream.print();
        env.execute("kafka job");
    }
}
