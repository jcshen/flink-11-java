package com.org.state;

import com.org.bean.SensorReading;
import org.apache.flink.contrib.streaming.state.RocksDBStateBackend;
import org.apache.flink.runtime.state.filesystem.FsStateBackend;
import org.apache.flink.runtime.state.memory.MemoryStateBackend;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 * @Authror: jcshen
 * @Date: 2021/1/1
 * @Version: 1.0
 * @Description:
 *        状态后端：
 *            特点：
 *                  每传入一条数据，有状态的算子任务都会读取和更新状态；
 *                  由于有效的状态访问对于处理数据的低延迟至关重要，因此每个并行任务都会在本地维护其状态，以确保快速的状态访问；
 *                  状态的存储、访问以及维护，由一个可插入的组件决定，这个组件就叫做状态后端（state backend）；
 *                  状态后端主要负责两件事：本地的状态管理，以及将检查点（chekpoint）状态写入远程存储；
 *            选择一个状态后端：
 *                  MemoryStateBackend
 *                      内存级的状态后端，会将键控状态作为内存中的对象进行管理，将它们存储在TaskManager的JVM堆上，
 *                      而将checkpoint存储在JobManager的内存中；
 *                      特点：快速，低延迟，但不稳定
 *                 FsStateBackend
 *                      将checkpoint存储到远程的持久化文件系统（FileSystem）上，而对于本地状态，跟MemoryStateBackend一样，
 *                      也会存在TaskManager的JVM堆上；
 *                      同时具有内存级的本地访问速度，和更好的容错保证；
 *                 RocksDBStateBackend
 *                      将所有状态序列化后，存入本地的RocksDB中存储；
 */
public class Backends_State {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        //1、状态后端配置
        env.setStateBackend(new MemoryStateBackend());
        env.setStateBackend(new FsStateBackend(""));
        env.setStateBackend(new RocksDBStateBackend(""));

        DataStream<String> inputStream = env.socketTextStream("localhost", 7777);
        DataStream<SensorReading> dataStream = inputStream.map(line -> {
            String[] fields = line.split(",");
            return new SensorReading(fields[0], new Long(fields[1]), new Double(fields[2]));
        });

        dataStream.print();

        env.execute("backends state job");
    }
}
