package com.org.state;

import com.org.bean.SensorReading;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.common.state.*;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 * @Authror: jcshen
 * @Date: 2020/12/31
 * @Version: 1.0
 * @Description:
 *          键控状态（Keyed State）
 *              特点：
 *                  键控状态是根据输入数据流中定义的键（key）来维护和访问的；
 *                  Flink为每个key维护一个状态实例，并将具有相同键的所有数据，都分区到同一个算子任务中，
 *                  这个任务会维护和处理这个key对应的状态；
 *                  当任务处理一条数据时，它会自动将状态的访问范围限定为当前数据的key；
 *              键控状态数据结构
 *                  值状态（Value state）
 *                      将状态表示为单个值；
 *                  列表状态（List state）
 *                      将状态表示为一组数据的列表；
 *                  映射状态（Map state）
 *                      将状态表示为一组Key-Value对
 *                  聚合状态（Reducing state & Aggregating state）
 *                      将状态表示为一个用于聚合操作的列表
 */
public class Keyed_State {
    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        DataStream<String> inputStream = env.socketTextStream("localhost", 7777);
        DataStream<SensorReading> dataStream = inputStream.map(line -> {
            String[] fields = line.split(",");
            return new SensorReading(fields[0], new Long(fields[1]), new Double(fields[2]));
        });
        //定义一个有状态的map操作，统计当前sensor数据个数
        SingleOutputStreamOperator<Integer> resultStream = dataStream.keyBy("id")
                .map(new KeyCountMapper());

        resultStream.print();

        env.execute("key state job");

    }

    /**
     * 自定义RichMapFunction
     */
    public static class KeyCountMapper extends RichMapFunction<SensorReading,Integer> {

        private ValueState<Integer> keyCountState;
        //其他类型状态的声明
        private ListState<String> listState;
        private MapState<String,Double> mapState;
        private ReducingState<SensorReading> reducingState;

        @Override
        public void open(Configuration parameters) throws Exception {//生命周期
            keyCountState=getRuntimeContext().getState(new ValueStateDescriptor<Integer>("key-count",Integer.class));
            //list state
            listState = getRuntimeContext().getListState(new ListStateDescriptor<String>("key-list",String.class));
            //map state
            mapState = getRuntimeContext().getMapState(new MapStateDescriptor<String, Double>("key-map",String.class,Double.class));
            //reducing state
//            getRuntimeContext().getReducingState(new ReducingStateDescriptor<Object>(SensorReading.class));
        }

        @Override
        public Integer map(SensorReading value) throws Exception {
            //其它状态api调用
            //list state
            for (String st : listState.get()) {
                System.out.println(st);
            }
            listState.add("hello");
            //map state
            mapState.get("1");
            mapState.put("2",34.4);
            //reducing state
            reducingState.add(value);
            
            //value state
            Integer count = keyCountState.value();
            count++;
            keyCountState.update(count);
            return count;
        }
    }
}
