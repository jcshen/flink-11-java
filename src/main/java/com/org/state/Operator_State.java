package com.org.state;

import com.org.bean.SensorReading;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.streaming.api.checkpoint.ListCheckpointed;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import java.util.Collections;
import java.util.List;

/**
 * @Authror: jcshen
 * @Date: 2020/12/31
 * @Version: 1.0
 * @Description:
 *      Flink中的状态
 *          算子状态（Operator State）
 *              特点：
 *                  算子状态的作用范围限定为算子任务，由同一并行任务所处理的所有数据都可以访问到相同的状态；
 *                  状态对于同一子任务而言是共享的；
 *                  算子状态不能由相同或不同算子的另一个子任务访问；
 *              算子状态数据结构：
 *                  列表状态（List state）
 *                      将状态表示为一组数据的列表；
 *                  联合列表状态（Union list state）
 *                      也将状态表示为数据的列表。它与常规列表状态的区别在于，在发生故障时，
 *                      或者从保存点（savepoint）启动应用程序时如何恢复
 *                  广播状态（Broadcast state）
 *                      如果一个算子有多项任务，而它的每项任务状态又都相同，那么这种特殊情况最合适应用广播状态
 *
 *
 *
 *          键控状态（Keyed State）
 *              根据输入数据流中定义的键（Key）来维护和访问
 *          状态后端（State Backends）
 *
 */
public class Operator_State {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        DataStream<String> inputStream = env.socketTextStream("localhost", 7777);
        DataStream<SensorReading> dataStream = inputStream.map(line -> {
            String[] fields = line.split(",");
            return new SensorReading(fields[0], new Long(fields[1]), new Double(fields[2]));
        });
        //定义一个有状态的map操作，统计当前分区数据个数
        SingleOutputStreamOperator<Integer> resultStream = dataStream.map(new CountMapper());
        resultStream.print();

        env.execute("state job");
    }

    /**
     * 自定义MapFunction
     */
    public static class CountMapper implements MapFunction<SensorReading,Integer>, ListCheckpointed<Integer> {
        //定义一个本地变量，作为算子状态
        private Integer count=0;

        @Override
        public Integer map(SensorReading value) throws Exception {
            count++;
            return count;
        }

        @Override
        public List<Integer> snapshotState(long checkpointId, long timestamp) throws Exception {
            //保存多个状态
            return Collections.singletonList(count);
        }

        @Override
        public void restoreState(List<Integer> state) throws Exception {
            //恢复多个状态
            for (Integer num : state) {
                count+=num;
            }

        }
    }
}
