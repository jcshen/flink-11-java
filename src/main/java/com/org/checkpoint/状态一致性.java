package com.org.checkpoint;

/**
 * @Authror: jcshen
 * @Date: 2021/1/1
 * @Version: 1.0
 * @Description:    状态一致性
 *      状态一致性
 *      一致性检查点（checkpoint）
 *      端到端（end-to-end）状态一致性
 *      端到端的精确一次（exactly-once）保证
 *      Flink + Kafka端到端状态一致性的保证
 *
 * 什么是状态一致性
 *       有状态的流处理，内部每个算子任务都可以有自己的状态；
 *       对于流处理器内部来说，所谓的状态一致性，其实就是我们所所的计算结果要保证准确；
 *       一条数据不应该丢失，也不应该重复计算；
 *       在遇到故障时可以恢复状态，恢复以后的重新计算，结果也是应该完全正确的；
 *
 * 状态一致性分类
 *      at-most-noce（最多一次）
 *          当任务故障时，最简单的做法是什么都不干，既不恢复丢失的状态，也不重播丢失的数据。
 *          at-most-once语义的含义是最多处理一次事件。
 *      at-least-once（至少一次）
 *          在大多数的真实应用场景，希望不丢失事件。这种类型的保障称为at-least-once，
 *          即所有的事件都得到处理，而一些事件还可能被处理多次。
 *      exactly-once（精确一次）
 *          恰好处理一次是最严格的保证，也是最难实现的。恰好处理一次语义意味着没有事件丢失，
 *          针对每一个数据，内部状态仅仅更新一次。
 *
 * 端到端exactly-once
 *      内部保证——checkpoint
 *      source端——可重设数据的读取位置
 *      sink端——从故障恢复时，数据不会重复写入外部系统
 *          幂等性写入
 *          事务写入
 *
 *      幂等写入 （IdempotentWrites）
 *          所谓幂等操作，是说一个操作，可以重复执行很多次，但只导致一次结果更改，即后面再重复执行就不起作用。
 *      事务写入（TransactionalWrites）
 *          事务（Transaction）
 *              应用程序中一系列严密操作，所有操作必须成功完成，否则在每个操作中所作的所有更改都会被撤销；
 *              具有原子性：一个事务中的一系列的操作要么全部成功，要么一个都不做；
 *          实现思想：构建的事务对应着checkpoint，等到checkpoint真正完成的时，才把所有对应的结果写入sink系统中；
 *          实现方式：
 *              预写日志（Write-Ahead-Log,wal）；
 *                      把结果数据先当成状态保存，然后再收到checkpoint完成的通知时，一次性吸入sink系统中；
 *                      简单易于实现，由于数据提前在状态后端中做了缓存，故无论什么sink系统，都能用这种方式搞得
 *                      DataStream API提供了一个模板类：GenericWriteAheadSin，来实现这种事务性sink；
 *
 *              两阶段提交（Two-Phase-Commit,2PC）；
 *                      对于每个checkpoint，sink任务会启动一个事务，并将接下来所有接收的数据添加到事务里；
 *                      然后将这些数据写入外部sink系统，但不提交它们——这时只是"与提交"；
 *                      当它收到checkpoint完成通知时，它才正式提交事务，实现结果的真正写入；
 *                      这种方式真正实现了exactly-once，它需要一个提供事务支持的外部sink系统。Flink提供了TwoPhaseCommitSinkFunction接口；
 *
 * Flink + Kafka端到端状态一致性保证
 *      内部——利用checkpoint机制，把状态存盘，发生故障时可恢复，保证内部状态一致性；
 *      source——Kafka consumer作为source，可将偏移量保存下来，如果任务出现故障，恢复时可由连接器重置偏移量，重新消费数据，保证一致性；
 *      sink——Kafka produce作为sink，采用两阶段提交sink，需要实现TwoPhaseCommitSinkFunction；
 *
 * Exactly-once两阶段提交
 *      Kafka——>DataSource——>Window——>DataSink——>Kafka
 *
 *      JobManager协调各个TaskManager进行checkpoint存储；
 *      checkpoint保存在StateBackend中，默认StateBackend是内存级的，可改为文件级的进行持久化保存；
 *      当checkpoint启动时，JobManager会将检查点分界线（barrier）注入数据流；
 *      barrier会在算子间传递下去；
 *      每个算子会对当前的状态做个快照，保存到状态后端；
 *      checkpoint机制可以保证内部的状态一致性；
 *      每个内部的transform任务遇到barrier时，都会把状态存到checkpoint里；
 *      sink任务首先把数据写入外部Kafka，这些数据都属于预提交的事务，遇到barrier时，把状态保存到状态后端，并开启新的预提交事务；
 *      当所有算子任务的快照完成，即这次的checkpoint完成时，JobManager会向所有任务发通知，确认这次checkpoint完成；
 *      sink任务收到确认通知，正式提交之前的事务，Kafka中未确认数据改为"已确认"；
 *
 *      Exactly-once两阶段提交步骤：
 *          第一条数据来后，开启一个Kafka的事务，正常写入Kafka分区日志但标记为未提交，这就是"预提交"；
 *          JobManager触发checkpoint操作，barrier从source开始向下传递，遇到barrier的算子将状态存入状态后端，并通知JobManager;
 *          sink连接器收到barrier，保存当前状态，存入checkpoint，通知JobManager，并开启下一阶段的事务，用于提交下个检查点的数据；
 *          JobManager收到所有任务的通知，发出确认信息，正式提交这段时间的数据；
 *          外部Kafka关闭事务，提交的数据可以正常消费了；
 *
 */
public class 状态一致性 {

}
