package com.org.checkpoint;

import com.org.bean.SensorReading;
import org.apache.flink.api.common.restartstrategy.RestartStrategies;
import org.apache.flink.api.common.time.Time;
import org.apache.flink.contrib.streaming.state.RocksDBStateBackend;
import org.apache.flink.runtime.state.filesystem.FsStateBackend;
import org.apache.flink.runtime.state.memory.MemoryStateBackend;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 * @Authror: jcshen
 * @Date: 2021/1/1
 * @Version: 1.0
 * @Description:    容错机制
 *          一致性检查点（checkpoint）
 *          从检查点恢复状态
 *          Flink检查点算法
 *          保存点（save points）
 *
 *一致性检查点（checkpoint）
 *          Flink故障恢复的核心，就是应用状态的一致性检查点。
 *          有状态流应用的一致检查点，其实就是所有任务的状态，在某个时间点的一份拷贝（一份快照）；
 *          这个时间点，应该是所有任务都恰好处理完一个相同的输入数据的时候。
 *                                  intput offset
 *                                      |
 *                  input stream----->Source
 *                                      |
 *                       Checkpoint--->storage
 *                       JobManager
 *
 * 从检查点恢复状态
 *          在执行流应用程序期间，Flink会定期保存状态的一致检查点
 *          如果发生故障，Flink将会使用最进的检查点来一致恢复应用程序的状态，并重新启动处理流程
 *          ---
 *          遇到故障之后，第一步是重启应用
 *          第二步是从checkpoint中读取状态，将状态重置
 *          从检查点重新启动应用程序后，其内部状态与检查点完成时的状态完全相同
 *          第三步开始消费并处理检查点到发生故障之间的所有数据
 *          这种检查点的保存和恢复机制可以为应用程序状态提供"精确一次"（exactly-once）的一致性，
 *          因为所有算子都会保存检查点并恢复其所有状态，这样一来所有的输入流就都会被重置到检查点完成时的位置
 *
 * Flink检查点算法
 *          一种简单的想法：暂停应用，保存状态到检查点，再重新恢复应用
 *          Flink的改进实现：
 *              ——基于Chandy-Lamport算法的分布式快照
 *              ——将检查点的保存和数据处理分离开，不暂停整个应用
 *         检查点分界线（Checkpoint Barrier）
 *              Flink的检查点算法用到了一种称为分界线（barrier）的特殊数据行式，用来把一条流上数据按照不同的检查点分开；
 *              分界线之前到来的数据导致的状态更改，都会被包含再当前分界线所属的检查点中，而基于分界线之后的数据导致的所有更改，
 *              就会被包含再之后的检查点中；
 *
 *              现在是一个有两个输入流的应用程序，用并行的两个Source任务来读取；
 *              JobManager会向每个source任务发送一条带有新检查点ID的消息，通过这种方式来启动检查点；
 *              数据源将它们的状态写入检查点，并发出一个检查点barrier；
 *              状态后端再状态存入检查点之后，会返回通知给source任务，source任务就会向JobManager确认检查点完成；
 *              分界线对齐：barrier向下传递，sum任务会等待所有输入分区的barrier到达；
 *              对于barrier已经到达的分区，继续到达的数据会被缓存；
 *              而barrier尚未到达的分区，数据会被正常处理；
 *              当收到所有分区的barrier时，任务就将其状态保存到状态后端的检查点中，然后将barrier继续向下游转发
 *              Sink任务向JobManager确认状态保存到checkpoint完毕；
 *              当所有任务都确认已将状态保存到检查点时，检查点就真正完成了；
 *
 * 保存点（save points）
 *          Flink还提供了可自定义的镜像保存功能，就是保存点（savepoints）；
 *          原则上，创建保存点使用的算法与检查点完全相同，因此保存点可以认为是具有额外元数据的检查点；
 *          Flink不会自动创建保存点，因此用户（或者外部调度程序）必须明确地触发创建操作；
 *          保存点是一个强大的功能。除了故障恢复外，保存点可用于：有计划的手动备份，更新应用程序，版本迁移，暂停和重启应用，等等；
 *
 *
 */
public class CheckPoint {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        //1、状态后端配置
        env.setStateBackend(new MemoryStateBackend());
        env.setStateBackend(new FsStateBackend(""));
        env.setStateBackend(new RocksDBStateBackend(""));
        //2、检查点配置
        env.enableCheckpointing(300);//默认500毫秒
        //checkpoint高级选项
        env.getCheckpointConfig().setCheckpointingMode(CheckpointingMode.EXACTLY_ONCE);//检查点模式
        env.getCheckpointConfig().setCheckpointTimeout(60000L);//检查点超时时间 1分钟
        env.getCheckpointConfig().setMaxConcurrentCheckpoints(2);//同时执行的检查点
        env.getCheckpointConfig().setMinPauseBetweenCheckpoints(100L);//前一次checkpoint的完成与下一次checkpoint的开始时间不能小于100毫秒
        env.getCheckpointConfig().setPreferCheckpointForRecovery(true);//默认false，用checkpoint恢复状态，而不是用更近的savepoint
        env.getCheckpointConfig().setTolerableCheckpointFailureNumber(0);//默认0，即checkpoint挂了，应用就挂了，容忍checkpoint失败的次数
        //3、重启策略配置
        env.setRestartStrategy(RestartStrategies.fixedDelayRestart(3,10000L));//固定延迟重启：重启3次，每隔10秒钟重启一次
        env.setRestartStrategy(RestartStrategies.failureRateRestart(3, Time.minutes(10),Time.minutes(1)));//失败率重启：10分钟内，重启3次，没次重启间隔1分钟



        DataStream<String> inputStream = env.socketTextStream("localhost", 7777);
        DataStream<SensorReading> dataStream = inputStream.map(line -> {
            String[] fields = line.split(",");
            return new SensorReading(fields[0], new Long(fields[1]), new Double(fields[2]));
        });

        dataStream.print();

        env.execute("backends state job");
    }
}
