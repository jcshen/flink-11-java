package com.org.sink;

import com.org.bean.SensorReading;
import org.apache.flink.connector.jdbc.JdbcConnectionOptions;
import org.apache.flink.connector.jdbc.JdbcExecutionOptions;
import org.apache.flink.connector.jdbc.JdbcSink;
import org.apache.flink.connector.jdbc.JdbcStatementBuilder;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @Authror: jcshen
 * @Date: 2020/12/27
 * @Version: 1.0
 * @Description: JDBC自定义Sink
 *  https://ci.apache.org/projects/flink/flink-docs-release-1.12/zh/dev/connectors/jdbc.html
 */
public class JDBC_Sink {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        DataStream<String> inputStream = env.readTextFile("F:\\workspace\\flink\\flink-11-java\\src\\main\\resources\\sensor.txt");
        DataStream<SensorReading> dataStream = inputStream.map(line -> {
            String[] fields = line.split(",");
            return new SensorReading(fields[0], new Long(fields[1]), new Double(fields[2]));
        });

        dataStream.addSink(
                JdbcSink.sink("insert into sensor_temp(id,ts,temp),values(?,?,?)",
                new JdbcStatementBuilder<SensorReading>(){
                    @Override
                    public void accept(PreparedStatement ps, SensorReading value) throws SQLException {
                        ps.setString(1,value.getId());
                        ps.setLong(2,value.getTimestamp());
                        ps.setDouble(3,value.getTemperature());
                        System.out.println("插入数据为："+value.getId()+","+value.getTimestamp()+","+value.getTemperature());
                    }
                },
                JdbcExecutionOptions.builder()
                        //默认batchSize是5000；batchSize是按线程划的，要一个线程的batchSize达到5才行，才会写入MySQL，否则不写入
                        .withBatchSize(5)
//                        .withBatchIntervalMs(3)
                        .build(),
                new JdbcConnectionOptions.JdbcConnectionOptionsBuilder()
                        .withUrl("jdbc:mysql://localhost:3306/flink?serverTimezone=UTC")
                        .withDriverName("com.mysql.jdbc.Driver")
                        .withUsername("root")
                        .withPassword("root123")
                        .build()
        ));

        env.execute("jdbc job");

    }
}
