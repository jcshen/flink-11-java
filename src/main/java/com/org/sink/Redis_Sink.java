package com.org.sink;

import com.org.bean.SensorReading;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.redis.RedisSink;
import org.apache.flink.streaming.connectors.redis.common.config.FlinkJedisPoolConfig;
import org.apache.flink.streaming.connectors.redis.common.mapper.RedisCommand;
import org.apache.flink.streaming.connectors.redis.common.mapper.RedisCommandDescription;
import org.apache.flink.streaming.connectors.redis.common.mapper.RedisMapper;

/**
 * @Authror: jcshen
 * @Date: 2020/12/27
 * @Version: 1.0
 * @Description:    往redis写入数据
 *  1、启动redis服务：redis-server
 *  2、启动程序
 *  3、查看redis中是否有数据写入
 *      redis-cli
 *      keys *
 *      根据key查询：hget sensor_temp sensor_1
 *      查询所有：hgetall sensor_temp
 */
public class Redis_Sink {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        DataStream<String> inputStream = env.readTextFile("F:\\workspace\\flink\\flink-11-java\\src\\main\\resources\\sensor.txt");
        DataStream<SensorReading> dataStream = inputStream.map(line -> {
            String[] fields = line.split(",");
            return new SensorReading(fields[0], new Long(fields[1]), new Double(fields[2]));
        });
        //定义jedis连接配置
        FlinkJedisPoolConfig config = new FlinkJedisPoolConfig.Builder()
                .setHost("localhost")
                .setPort(6379)
                .build();

        dataStream.addSink(new RedisSink<>(config,
                new RedisMapper<SensorReading>(){//自定义RedisMapper

            //定义保存数据到redis的命令，存成Hash表，hset sensor_temp id temperature
            @Override
            public RedisCommandDescription getCommandDescription() {
                //数据类型，表名
                return new RedisCommandDescription(RedisCommand.HSET,"sensor_temp");
            }
            //要保存的key--->id
            @Override
            public String getKeyFromData(SensorReading data) {
                return data.getId();
            }
            //要保存的值--->temperature，温度值
            @Override
            public String getValueFromData(SensorReading data) {
                return data.getTemperature().toString();
            }
        }));

        env.execute("redis sink job");

    }
}
