package com.org.sink;

import com.org.bean.SensorReading;
import com.org.source.SourceUdf;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;
import org.apache.flink.streaming.api.functions.sink.SinkFunction;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

/**
 * @Authror: jcshen
 * @Date: 2020/12/27
 * @Version: 1.0
 * @Description:    自定义JDBC sink
 *  1、MySQL创建表：create table sensor_temp(id varchar(20) not null,temp double not null);
 *  2、启动程序
 *  3、查看MySQL的结果
 */
public class JDBCSink_UDF {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

//        DataStream<String> inputStream = env.readTextFile("F:\\workspace\\flink\\flink-11-java\\src\\main\\resources\\sensor.txt");
//        DataStream<SensorReading> dataStream = inputStream.map(line -> {
//            String[] fields = line.split(",");
//            return new SensorReading(fields[0], new Long(fields[1]), new Double(fields[2]));
//        });
        //使用自定义模拟Source数据
        DataStreamSource<SensorReading> dataStream = env.addSource(new SourceUdf.SensorSource());

        dataStream.addSink(new JDBCSink());
        env.execute("jdbc udf job");

    }

    /**
     * 实现自定义的SinkFunction
     */
    public static class JDBCSink extends RichSinkFunction<SensorReading> {
        //声明连接和预编译语句
        Connection connection = null;
        PreparedStatement insertStmt = null;
        PreparedStatement updateStmt = null;

        @Override
        public void open(Configuration params) throws Exception {
            connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/flink?serverTimezone=UTC","root","root123");
            insertStmt = connection.prepareStatement("insert into sensor_temp(id,temp) values(?,?)");
            updateStmt = connection.prepareStatement("update sensor_temp set temp = ? where id = ?");
        }

        //每来一条数据，调用连接，执行SQL
        @Override
        public void invoke(SensorReading value, Context context) throws Exception {
            //先执行更新，若没有更新就插入
            updateStmt.setDouble(1,value.getTemperature());
            updateStmt.setString(2,value.getId());
            updateStmt.execute();

            if (updateStmt.getUpdateCount()==0){
                insertStmt.setString(1,value.getId());
                insertStmt.setDouble(2,value.getTemperature());
                insertStmt.execute();
            }

        }

        //关闭连接
        @Override
        public void close() throws Exception {
            insertStmt.close();
            updateStmt.close();
            connection.close();
        }
    }
}
