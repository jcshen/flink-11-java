package com.org.sink;

import com.org.bean.SensorReading;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer011;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer011;

import java.util.Properties;

/**
 * @Authror: jcshen
 * @Date: 2020/12/27
 * @Version: 1.0
 * @Description:    Kafka数据管道；从Kafka进，从Kafka出
 *  1、启动Kafka生产者：bin/kafka-console-producer.sh --broker-list localhost:9092 --topic sensor
 *  2、启动Kafka消费者：bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic sink_sensor
 *  3、启动程序，在Kafka生产者中输入数据，查看Kafka消费者中是否有数据
 */
public class Kafka2Kafka {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers","localhost:9092");
        properties.setProperty("group.id","consumer-group");
        properties.setProperty("key.deserializer","org.apache.kafka.common.serializtion.StringDeserializer");
        properties.setProperty("value.deserializer","org.apache.kafka.common.serializtion.StringDeserializer");
        properties.setProperty("auto.offset.reset","latest");
        //Kafka source
        DataStream<String> inputStream = env.addSource(new FlinkKafkaConsumer011<String>("sensor", new SimpleStringSchema(), properties));
        DataStream<String> dataStream = inputStream.map(line -> {
            String[] fields = line.split(",");
            return new SensorReading(fields[0], new Long(fields[1]), new Double(fields[2])).toString();
        });
        //kafka sink
        dataStream.addSink(new FlinkKafkaProducer011<String>("localhost:9092","sink_sensor",new SimpleStringSchema()));

        env.execute("kafka to kafka job");
    }
}
