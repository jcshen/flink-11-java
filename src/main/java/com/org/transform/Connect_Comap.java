package com.org.transform;

import com.org.bean.SensorReading;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.streaming.api.collector.selector.OutputSelector;
import org.apache.flink.streaming.api.datastream.*;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.co.CoMapFunction;

import java.util.Collections;

/**
 * @Authror: jcshen
 * @Date: 2020/12/26
 * @Version: 1.0
 * @Description:
 *  Connect
 *      DataStream，DataStream--->ConnectedStream
 *      连接两个保持它们类型的数据流，两个数据流被Connect之后，只是被放在了同一个流中，
 *      内部依然保持各自的数据和行式不发生任何变化，两个流相互独立。
 *  CoMap,CoFlatMap
 *      ConnectedStreams--->DataStream
 *      作用于ConnectedStreams上，功能与map和flatMap一样，对ConnectedStreams中的每一个
 *      Stream分别进行map和flatMap处理。
 */
public class Connect_Comap {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        DataStream<String> inputStream = env.readTextFile("F:\\workspace\\flink\\flink-11-java\\src\\main\\resources\\sensor.txt");
        DataStream<SensorReading> dataStream = inputStream.map(line -> {
            String[] fields = line.split(",");
            return new SensorReading(fields[0], new Long(fields[1]), new Double(fields[2]));
        });
        //拆流
        SplitStream<SensorReading> splitStream = dataStream.split(new OutputSelector<SensorReading>() {
            @Override
            public Iterable<String> select(SensorReading value) {
                //依据30度为边界，分流
                return (value.getTemperature() > 30 ? Collections.singletonList("high") : Collections.singletonList("low"));
            }
        });
        //获取流
        DataStream<SensorReading> highStream = splitStream.select("high");
        DataStream<SensorReading> lowStream = splitStream.select("low");
        //将高温流转换成二元组类型
        DataStream<Tuple2<String, Double>> warnStream = highStream.map(new MapFunction<SensorReading, Tuple2<String, Double>>() {
            @Override
            public Tuple2<String, Double> map(SensorReading value) throws Exception {
                return new Tuple2<String, Double>(value.getId(), value.getTemperature());
            }
        });
        //合流 connect，高温转换流与低温流连接合并之后，输出状态信息
        ConnectedStreams<Tuple2<String, Double>, SensorReading> connectStream = warnStream.connect(lowStream);
        DataStream<Object> connectedStream = connectStream.map(new CoMapFunction<Tuple2<String, Double>, SensorReading, Object>() {
            @Override
            public Object map1(Tuple2<String, Double> value) throws Exception {
                return new Tuple3<String, Double, String>(value.f0, value.f1, "high temp warning");
            }

            @Override
            public Object map2(SensorReading value) throws Exception {
                return new Tuple2<String, String>(value.getId(), "normal");
            }
        });
        connectedStream.print();
        env.execute("connect job");
    }
}
