package com.org.transform;

import com.org.bean.SensorReading;
import org.apache.flink.streaming.api.collector.selector.OutputSelector;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SplitStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import java.util.Collections;

/**
 * @Authror: jcshen
 * @Date: 2020/12/26
 * @Version: 1.0
 * @Description:
 *  Union
 *      DataStream--->DataStream
 *      对两个或者两个以上的DataStream进行union操作，产生一个包含所有DataStream元素的新DataStream。
 */
public class UnionTrans {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        DataStream<String> inputStream = env.readTextFile("F:\\workspace\\flink\\flink-11-java\\src\\main\\resources\\sensor.txt");
        SplitStream<SensorReading> splitStream = inputStream.map(line -> {
            String[] fields = line.split(",");
            return new SensorReading(fields[0], new Long(fields[1]), new Double(fields[2]));
        })
                .split(new OutputSelector<SensorReading>() {
                    @Override
                    public Iterable<String> select(SensorReading value) {
                        return (value.getTemperature() > 30 ? Collections.singletonList("high") : Collections.singletonList("low"));
                    }
                });

        DataStream<SensorReading> highStream = splitStream.select("high");
        DataStream<SensorReading> lowStream = splitStream.select("low");

        DataStream<SensorReading> unionStream = highStream.union(lowStream);
        unionStream.print();
        env.execute("union job");
    }
}
