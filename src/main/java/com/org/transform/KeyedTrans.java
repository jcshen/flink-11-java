package com.org.transform;

import com.org.bean.SensorReading;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 * @Authror: jcshen
 * @Date: 2020/12/26
 * @Version: 1.0
 * @Description:
 *  DataStream--->KeyedStream
 *  逻辑的将一个流拆分成不相交的分区，每个分区含具有相同key的元素，在内部以hash的行式实现。
 *  滚动聚合算子（Rolling Aggregation）
 *  这些算子针对KeyedStream的每一个支流做聚合。
 *  sum()
 *  min()
 *  max()
 *  minBy()
 *  maxBy()
 *
 *  Reduce
 *  KeyedStream--->DataStream
 *  一个分组数据流的聚合操作，合并当前的元素和上次聚合的结果，产生一个新的值，
 *  返回的流中包含每一次聚合的结果，而不是只返回最后一次聚合的最终结果。
 */
public class KeyedTrans {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        //从文件读取数据
        DataStream<String> inputStream = env.readTextFile("F:\\workspace\\flink\\flink-11-java\\src\\main\\resources\\sensor.txt");
        //转换成SensorReading类型
//        DataStream<SensorReading> sensorStream = inputStream.map(new MapFunction<String, SensorReading>() {
//            public SensorReading map(String value) throws Exception {
//                String[] fields = value.split(",");
//                return new SensorReading(fields[0], new Long(fields[1]), new Double(fields[2]));
//            }
//        });
        DataStream<SensorReading> mapStream = inputStream.map(line -> {
            String[] fields = line.split(",");
            return new SensorReading(fields[0], new Long(fields[1]), new Double(fields[2]));
        });
        //分组
        KeyedStream<SensorReading, Tuple> keyedStream = mapStream.keyBy("id");
//        KeyedStream<SensorReading, String> keyedStream = mapStream.keyBy(SensorReading::getId);
        //滚动聚合，取当前最大的温度值
//        DataStream<SensorReading> resultStream = keyedStream.max("temperature");
        SingleOutputStreamOperator<SensorReading> resultStream = keyedStream.maxBy("temperature");
        //打印输出
        resultStream.print("max");
        env.execute("rolling aggregation job");
    }

}
