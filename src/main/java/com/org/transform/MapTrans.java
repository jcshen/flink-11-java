package com.org.transform;

import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.util.Collector;

/**
 * @Authror: jcshen
 * @Date: 2020/12/26
 * @Version: 1.0
 * @Description:
 */
public class MapTrans {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        //从文件读取数据
        DataStream<String> intputStrream = env.readTextFile("F:\\workspace\\flink\\flink-11-java\\src\\main\\resources\\sensor.txt");
        //1、map，把String转换长度输出
        DataStream<Integer> mapStream = intputStrream.map(new MapFunction<String, Integer>() {
            public Integer map(String s) throws Exception {
                return s.length();
            }
        });
        //2、flatMap，分割字段输出
        DataStream<String> flatStream = intputStrream.flatMap(new FlatMapFunction<String, String>() {
            public void flatMap(String value, Collector<String> out) throws Exception {
                String[] fields = value.split(",");
                for (String field : fields) {
                    out.collect(field);
                }
            }
        });
        //3、filter，过滤，返回false就是不要，true,就是要
        DataStream<String> filteStream = intputStrream.filter(new FilterFunction<String>() {
            public boolean filter(String value) throws Exception {
                return value.startsWith("sensor_1");
            }
        });
        //打印输出
        mapStream.print("map");
        flatStream.print("flat");
        filteStream.print("filter");

        env.execute("transform job");
    }

}
