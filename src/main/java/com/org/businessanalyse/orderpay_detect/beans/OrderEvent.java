package com.org.businessanalyse.orderpay_detect.beans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @Authror: jcshen
 * @Date: 2021/1/31
 * @Version: 1.0
 * @Description:
 */
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Data
public class OrderEvent {
    private Long orderId;
    private String eventType;
    private String txId;
    private Long timestamp;
}
