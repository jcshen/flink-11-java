package com.org.businessanalyse.orderpay_detect.beans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @Authror: jcshen
 * @Date: 2021/2/5
 * @Version: 1.0
 * @Description:    订单支付实时对账
 */
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Data
public class ReceiptEvent {
    private String txId;
    private String payChannel;
    private Long timestamp;
}
