package com.org.businessanalyse.orderpay_detect.beans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @Authror: jcshen
 * @Date: 2021/1/31
 * @Version: 1.0
 * @Description:
 */
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Data
public class OrderResult {
    private Long orderId;
    private String resultState;
}
