package com.org.businessanalyse.market_analysis;

import com.org.businessanalyse.market_analysis.beans.ChannelPromotionCount;
import com.org.businessanalyse.market_analysis.beans.MarketUseraBehavior;
import org.apache.flink.api.common.functions.AggregateFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.AscendingTimestampExtractor;
import org.apache.flink.streaming.api.functions.source.SourceFunction;
import org.apache.flink.streaming.api.functions.windowing.ProcessWindowFunction;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * @Authror: jcshen
 * @Date: 2021/1/31
 * @Version: 1.0
 * @Description:    市场推广不分渠道统计
 */
public class AppMarketStatistics {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        //1、从自定义数据源中读取数据
        DataStream<MarketUseraBehavior> dataStream = env.addSource(new SimulatedMarketUserBehaviorSource())
                .assignTimestampsAndWatermarks(new AscendingTimestampExtractor<MarketUseraBehavior>() {
                    @Override
                    public long extractAscendingTimestamp(MarketUseraBehavior element) {
                        return element.getTimestamp();
                    }
                });
        //2、开窗统计总量
        DataStream<ChannelPromotionCount> resultStream = dataStream
                .filter(data -> !"UNINSTALL".equals(data.getBehavior()))
                .map(new MapFunction<MarketUseraBehavior, Tuple2<String,Long>>() {
                    @Override
                    public Tuple2<String, Long> map(MarketUseraBehavior value) throws Exception {
                        return new Tuple2<String,Long>("total",1L);
                    }
                })
                .keyBy(0)//组合key、组合键
                .timeWindow(Time.hours(1), Time.seconds(5))//定义滑窗
                //预聚合增量聚合函数，全窗口函数
                .aggregate(new MarketingStatisticsAgg(), new MarketingStatisticsResult());
        //打印，5秒输出一次
        resultStream.print();
        //执行
        env.execute("app marketing by channel job");
    }

    /**
     * 实现自定义的模拟市场用户行为数据源
     */
    public static class SimulatedMarketUserBehaviorSource implements SourceFunction<MarketUseraBehavior> {
        //控制是否运行的标识
        Boolean running = true;
        //定义用户行为和渠道范围
        List<String> behaviors = Arrays.asList("CLICK","DOWNLOAD","INSTALL","UNINSTALL");
        List<String> channels = Arrays.asList("app store","wechat","weibo");
        Random random = new Random();

        @Override
        public void run(SourceContext<MarketUseraBehavior> ctx) throws Exception {
            while (running){
                //随机生成所有字段
                long id = random.nextLong();
                String behavior = behaviors.get(random.nextInt(behaviors.size()));
                String channel = channels.get(random.nextInt(channels.size()));
                long timestamp = System.currentTimeMillis();
                //发出数据
                ctx.collect(new MarketUseraBehavior(id,behavior,channel,timestamp));
                //控制输出频率
                Thread.sleep(100L);
            }
        }

        @Override
        public void cancel() {
            running = false;
        }
    }

    /**
     *
     */
    public static class MarketingStatisticsAgg implements AggregateFunction<Tuple2<String,Long>,Long,Long>{
        @Override
        public Long createAccumulator() {
            return 0L;
        }

        @Override
        public Long add(Tuple2<String, Long> value, Long accumulator) {
            return accumulator + 1;
        }

        @Override
        public Long getResult(Long accumulator) {
            return accumulator;
        }

        @Override
        public Long merge(Long a, Long b) {
            return a + b;
        }
    }

    public static class MarketingStatisticsResult implements WindowFunction<Long,ChannelPromotionCount, Tuple,TimeWindow> {
        @Override
        public void apply(Tuple tuple, TimeWindow window, Iterable<Long> input, Collector<ChannelPromotionCount> out) throws Exception {
            String windowEnd = new Timestamp(window.getEnd()).toString();
            Long count = input.iterator().next();

            out.collect(new ChannelPromotionCount("total","total",windowEnd,count));
        }
    }
}
