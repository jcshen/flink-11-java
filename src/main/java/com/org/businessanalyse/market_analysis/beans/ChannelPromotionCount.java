package com.org.businessanalyse.market_analysis.beans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @Authror: jcshen
 * @Date: 2021/1/31
 * @Version: 1.0
 * @Description:
 */
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Data
public class ChannelPromotionCount {
    private String channel;
    private String behavior;
    private String windowEnd;
    private Long count;
}
