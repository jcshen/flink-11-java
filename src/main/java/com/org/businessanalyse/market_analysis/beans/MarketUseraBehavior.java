package com.org.businessanalyse.market_analysis.beans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @Authror: jcshen
 * @Date: 2021/1/31
 * @Version: 1.0
 * @Description:
 */
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Data
public class MarketUseraBehavior {
    private Long userId;
    private String behavior;
    private String channel;
    private Long timestamp;
}
