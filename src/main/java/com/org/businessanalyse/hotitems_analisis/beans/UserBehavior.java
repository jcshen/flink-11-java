package com.org.businessanalyse.hotitems_analisis.beans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @Authror: jcshen
 * @Date: 2021/1/24
 * @Version: 1.0
 * @Description:
 */
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Data
public class UserBehavior {
    //定义私有属性
    private Long userId;
    private Long itemId;
    private Integer categoryId;
    private String behavior;
    private Long timestamp;
}
