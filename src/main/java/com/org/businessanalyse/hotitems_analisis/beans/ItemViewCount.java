package com.org.businessanalyse.hotitems_analisis.beans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @Authror: jcshen
 * @Date: 2021/1/24
 * @Version: 1.0
 * @Description:
 */
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Data
public class ItemViewCount {
    private Long itemId;
    private Long windowEnd;
    private Long count;
}
