package com.org.businessanalyse.loginfail_detect.beans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @Authror: jcshen
 * @Date: 2021/1/31
 * @Version: 1.0
 * @Description:
 */
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Data
public class LoginFailWarning {
    private Long userId;
    private Long firstFailTime;
    private Long lastFailTime;
    private String warningMsg;
}
