package com.org.businessanalyse.loginfail_detect;

import akka.event.Logging;
import com.org.businessanalyse.loginfail_detect.beans.LoginEvent;
import com.org.businessanalyse.loginfail_detect.beans.LoginFailWarning;
import org.apache.flink.api.common.state.ListState;
import org.apache.flink.api.common.state.ListStateDescriptor;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.calcite.shaded.com.google.common.collect.Lists;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.streaming.api.functions.timestamps.BoundedOutOfOrdernessTimestampExtractor;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.util.Collector;

import java.net.URL;
import java.util.ArrayList;

/**
 * @Authror: jcshen
 * @Date: 2021/1/31
 * @Version: 1.0
 * @Description:    恶意登录检测
 *              存在的问题：第一次登录失败后，注册定时器，一直等到2秒后才会报警；
 *              如果在2秒内进登录了N次，可能已经登录成功了；这样程序就会清空状态，不会报警了；
 *              这样就失去了实效性
 *
 */
public class LoginFail {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        //1、从文件中读取数据
        URL resource = LoginFail.class.getResource("/data/LoginLog.csv");
        DataStream<LoginEvent> loginEventStream = env.readTextFile(resource.getPath())
                .map(line -> {
                    String[] fields = line.split(",");
                    return new LoginEvent(new Long(fields[0]), fields[1], fields[2], new Long(fields[3]));
                })
                .assignTimestampsAndWatermarks(new BoundedOutOfOrdernessTimestampExtractor<LoginEvent>(Time.seconds(3)) {//延迟3秒，乱序数据
                    @Override
                    public long extractTimestamp(LoginEvent element) {
                        return element.getTimestamp() * 1000L;
                    }
                });
        //自定义处理函数检测连续登录失败事件
        DataStream<LoginFailWarning> warningStream = loginEventStream
                .keyBy(LoginEvent::getUserId)
                .process(new LoginFailDetectWarning(2));

        warningStream.print();
        env.execute("login fail detect job");


    }

    /**
     * 实现自定义KeyedProcessFunction
     */
    public static class LoginFailDetectWarning extends KeyedProcessFunction<Long,LoginEvent, LoginFailWarning> {
        //定义属性，最大连续登录失败次数
        private Integer maxFailTimes;

        public LoginFailDetectWarning(Integer maxFailTimes) {
            this.maxFailTimes = maxFailTimes;
        }

        //定义状态：保存2秒内所有的登录失败事件
        ListState<LoginEvent> loginFailEventListState;
        //定义状态：保存注册的定时器时间戳
        ValueState<Long> timerTsState;

        @Override
        public void open(Configuration parameters) throws Exception {
            //无初始值
            loginFailEventListState = getRuntimeContext().getListState(new ListStateDescriptor<LoginEvent>("login-fail-list",LoginEvent.class));
            timerTsState = getRuntimeContext().getState(new ValueStateDescriptor<Long>("timer-ts",Long.class));
        }

        @Override
        public void processElement(LoginEvent value, Context ctx, Collector<LoginFailWarning> out) throws Exception {
            //判断当前登录事件类型
            if ("fail".equals(value.getState())){
                //1、如果是失败事件，添加到列表状态中
                loginFailEventListState.add(value);
                //如果没有定时器，注册一个2秒之后的定时器
                if (timerTsState.value() == null){
                    Long ts = (value.getTimestamp() + 2) * 1000L;
                    ctx.timerService().registerEventTimeTimer(ts);
                    timerTsState.update(ts);
                }
            }else{
                //2、如果是登录成功，删除定时器，清空状态，重新开始
                if (timerTsState.value() != null){
                    ctx.timerService().deleteEventTimeTimer(timerTsState.value());
                }
                //清空状态
                loginFailEventListState.clear();
                timerTsState.clear();
            }

        }

        @Override
        public void onTimer(long timestamp, OnTimerContext ctx, Collector<LoginFailWarning> out) throws Exception {
            //定时器触发，说明2秒内没有登录成功，判断ListState中失败的个数
            ArrayList<LoginEvent> loginFailEvents = Lists.newArrayList(loginFailEventListState.get());
            Integer failTimes = loginFailEvents.size();

            if (failTimes >= maxFailTimes){
                //如果超出设定的最大失败次数，输出报警
                out.collect(new LoginFailWarning(
                        ctx.getCurrentKey(),//userId
                        loginFailEvents.get(0).getTimestamp(),//第一次登录失败的时间
                        loginFailEvents.get(failTimes - 1).getTimestamp(),//最后一次登录失败的时间
                        "login fail in 2s for " + failTimes + " times"
                ));
            }

            //清空状态
            loginFailEventListState.clear();
            timerTsState.clear();
        }

    }
}
