package com.org.businessanalyse.networkflow_analisys;

import com.org.businessanalyse.networkflow_analisys.bean.ApacheLogEvent;
import com.org.businessanalyse.networkflow_analisys.bean.PageViewCount;
import org.apache.flink.api.common.functions.AggregateFunction;
import org.apache.flink.api.common.state.ListState;
import org.apache.flink.api.common.state.ListStateDescriptor;
import org.apache.flink.api.common.state.MapState;
import org.apache.flink.api.common.state.MapStateDescriptor;
import org.apache.flink.calcite.shaded.com.google.common.collect.Lists;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.streaming.api.functions.timestamps.BoundedOutOfOrdernessTimestampExtractor;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;
import org.apache.flink.util.OutputTag;

import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * @Authror: jcshen
 * @Date: 2021/1/26
 * @Version: 1.0
 * @Description:    热门页面统计
 */
public class HotPages {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        env.setParallelism(1);
        //读取文件，转换成POJO
//        URL resource = HotPages.class.getResource("/data/apache.log");
//        DataStream<String> inputStream = env.readTextFile(resource.getPath());

        DataStreamSource<String> inputStream = env.socketTextStream("localhost", 7777);//data/test/NetworkFlowTest.csv

        DataStream<ApacheLogEvent> dataStream = inputStream
                .map(line -> {
                    String[] fields = line.split(" ");
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy:HH:mm:ss");
                    long timestamp = dateFormat.parse(fields[3]).getTime();
                    return new ApacheLogEvent(fields[0], fields[1], timestamp, fields[5], fields[6]);
                })//定义时间语义和watermark
                .assignTimestampsAndWatermarks(new BoundedOutOfOrdernessTimestampExtractor<ApacheLogEvent>(Time.seconds(1)) {//延迟1秒
                    @Override
                    public long extractTimestamp(ApacheLogEvent element) {
                        return element.getTimestamp();
                    }
                });

        dataStream.print("data");

        //定义一个侧输出流标签，处理乱序数据
        OutputTag<ApacheLogEvent> lateTag = new OutputTag<ApacheLogEvent>("late"){};
        //分组开窗聚合
        SingleOutputStreamOperator<PageViewCount> windowAggStream = dataStream.filter(data -> "GET".equals(data.getMethod()))//过滤get请求
               .filter(data ->{
                   String regex = "^((?!\\.(css|js|png|ico)$).)*$";
                   return Pattern.matches(regex,data.getUrl());
               })
                .keyBy(ApacheLogEvent::getUrl)//按照url分组
                .timeWindow(Time.minutes(10), Time.seconds(5))
                .allowedLateness(Time.minutes(1))//延迟1分钟，更新窗口数据，处理乱序数据
                .sideOutputLateData(lateTag)//侧输出流
                .aggregate(new PageCountAgg(), new PageCountResult());
        //
        windowAggStream.print("agg");
        windowAggStream.getSideOutput(lateTag).print("late");

        //收集同一窗口count数据，排序输出
        DataStream<String> resultStream = windowAggStream
                .keyBy(PageViewCount::getWindowEnd)
                .process(new TopNHotPages(3));
        //打印
        resultStream.print();

        env.execute("hot pages job");
    }

    /**
     * 自定义预聚合函数
     */
    public static class PageCountAgg implements AggregateFunction<ApacheLogEvent,Long,Long>{
        @Override
        public Long createAccumulator() {
            return 0L;
        }

        @Override
        public Long add(ApacheLogEvent value, Long accumulator) {
            return accumulator + 1;
        }

        @Override
        public Long getResult(Long accumulator) {
            return accumulator;
        }

        @Override
        public Long merge(Long a, Long b) {
            return a + b;
        }
    }

    /**
     * 自定义的窗口函数
     */
    public static class PageCountResult implements WindowFunction<Long,PageViewCount,String,TimeWindow> {
        @Override
        public void apply(String url, TimeWindow window, Iterable<Long> input, Collector<PageViewCount> out) throws Exception {
            out.collect(new PageViewCount(url,window.getEnd(),input.iterator().next()));
        }
    }

    /**
     * 自定义的处理函数
     */
    public static class TopNHotPages extends KeyedProcessFunction<Long,PageViewCount,String> {
        private Integer topSize;
        public TopNHotPages(Integer topSize) {
            this.topSize=topSize;
        }
        //定义状态，保存当前所有PageViewCount到List中，当有延迟数据时，需要遍历List，不方便
//        ListState<PageViewCount> pageViewCountListState;

        //定义状态，保存当前所有PageViewCount到Map中
        MapState<String,Long> pageViewCountMapState;

        @Override
        public void open(Configuration parameters) throws Exception {
//            pageViewCountListState = getRuntimeContext().getListState(new ListStateDescriptor<PageViewCount>("page-count-list",PageViewCount.class));
            pageViewCountMapState=getRuntimeContext().getMapState(new MapStateDescriptor<String, Long>("page-count-map",String.class,Long.class));
        }

        @Override
        public void processElement(PageViewCount value, Context ctx, Collector<String> out) throws Exception {
//            pageViewCountListState.add(value);

            pageViewCountMapState.put(value.getUrl(),value.getCount());
            ctx.timerService().registerEventTimeTimer(value.getWindowEnd() + 1);
            //注册一个1分钟后的定时器，用来清空状态
            ctx.timerService().registerEventTimeTimer(value.getWindowEnd() + 60 *1000L);//1分钟后
        }

        @Override
        public void onTimer(long timestamp, OnTimerContext ctx, Collector<String> out) throws Exception {
//            ArrayList<PageViewCount> pageViewCounts = Lists.newArrayList(pageViewCountListState.get());
            //排序
//            pageViewCounts.sort(new Comparator<PageViewCount>() {
//                @Override
//                public int compare(PageViewCount o1, PageViewCount o2) {
//                    if (o1.getCount() > o2.getCount())
//                        return -1;
//                    else if(o1.getCount() < o2.getCount())
//                        return 1;
//                    else
//                        return 0;
//                }
//            });
            //输出格式化
//            StringBuilder resultBuilder = new StringBuilder();
//            resultBuilder.append("--------------------------\n");
//            resultBuilder.append("窗口结束时间：").append(new Timestamp(timestamp - 1)).append("\n");
            //遍历List，取top n输出
//            for (int i = 0; i < Math.min(topSize, pageViewCounts.size()); i++) {
//                PageViewCount currentPageCount = pageViewCounts.get(i);
//                resultBuilder.append("NO ").append(i+1).append(":")
//                        .append(" 页面URL = ").append(currentPageCount.getUrl())
//                        .append(" 浏览量 = ").append(currentPageCount.getCount())
//                        .append("\n");
//            }

            //List时，清空状态，避免重复计算
//            pageViewCountListState.clear();

            //Map时，先判断是否到了窗口关闭清理时间，如果是，直接清空状态返回
            if (timestamp == ctx.getCurrentKey() + 60 * 1000L){
                pageViewCountMapState.clear();
                return;
            }

            StringBuilder resultBuilder = new StringBuilder();
            resultBuilder.append("--------------------------\n");
            resultBuilder.append("窗口结束时间：").append(new Timestamp(timestamp - 1)).append("\n");

            ArrayList<Map.Entry<String, Long>> pageViewCounts = Lists.newArrayList(pageViewCountMapState.entries());
            pageViewCounts.sort(new Comparator<Map.Entry<String, Long>>() {
                @Override
                public int compare(Map.Entry<String, Long> o1, Map.Entry<String, Long> o2) {
                    if (o1.getValue() > o2.getValue())
                        return -1;
                    else if (o1.getValue() < o2.getValue())
                        return 1;
                    else
                        return 0;
                }
            });
            //遍历Map，取top n输出
            for (int i = 0; i < Math.min(topSize, pageViewCounts.size()); i++) {
                Map.Entry<String, Long> currentPageCount = pageViewCounts.get(i);
                resultBuilder.append("NO ").append(i+1).append(":")
                        .append(" 页面URL = ").append(currentPageCount.getKey())
                        .append(" 浏览量 = ").append(currentPageCount.getValue())
                        .append("\n");
            }

            resultBuilder.append("------------------------\n\n");

            //控制输出频率
            Thread.sleep(1000L);

            out.collect(resultBuilder.toString());

        }
    }
}
