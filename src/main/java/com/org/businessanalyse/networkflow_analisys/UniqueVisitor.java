package com.org.businessanalyse.networkflow_analisys;

import com.org.businessanalyse.hotitems_analisis.beans.UserBehavior;
import com.org.businessanalyse.networkflow_analisys.bean.PageViewCount;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.AscendingTimestampExtractor;
import org.apache.flink.streaming.api.functions.windowing.AllWindowFunction;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

import java.net.URL;
import java.util.HashSet;

/**
 * @Authror: jcshen
 * @Date: 2021/1/30
 * @Version: 1.0
 * @Description: UV统计：独立访客数
 */
public class UniqueVisitor {
    public static void main(String[] args) throws Exception {
        //创建执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        //读取数据，创建DataStream
        URL resource = UniqueVisitor.class.getResource("/data/UserBehavior.csv");
        DataStream<String> inputStream = env.readTextFile(resource.getPath());
        //转换为POJO,分配时间戳盒watermark
        DataStream<UserBehavior> dataStream = inputStream
                .map(line -> {
                    String[] fields = line.split(",");
                    return new UserBehavior(new Long(fields[0]), new Long(fields[1]), new Integer(fields[2]), fields[3], new Long(fields[4]));
                })
                .assignTimestampsAndWatermarks(new AscendingTimestampExtractor<UserBehavior>() {
                    @Override
                    public long extractAscendingTimestamp(UserBehavior element) {
                        return element.getTimestamp() * 1000L;
                    }
                });
        //开窗统计uv值
        DataStream<PageViewCount> uvStream = dataStream
                .filter(data -> "pv".equals(data.getBehavior()))
                .timeWindowAll(Time.hours(1))//不建议使用
                .apply(new UvCountResult());

        uvStream.print();

        env.execute("uv count job");
    }

    //实现自定义全窗口函数
    public static class UvCountResult implements AllWindowFunction<UserBehavior,PageViewCount, TimeWindow> {

        @Override
        public void apply(TimeWindow window, Iterable<UserBehavior> values, Collector<PageViewCount> out) throws Exception {
            //定义一个Set结构，保存窗口中的所有userId，自动去重
            HashSet<Long> uidSet = new HashSet<>();
            for (UserBehavior ub:values) {
                uidSet.add(ub.getUserId());
                out.collect(new PageViewCount("uv",window.getEnd(), (long) uidSet.size()));
            }

        }
    }
}
