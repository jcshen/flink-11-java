package com.org.businessanalyse.networkflow_analisys;

import com.org.businessanalyse.hotitems_analisis.beans.UserBehavior;
import com.org.businessanalyse.networkflow_analisys.bean.PageViewCount;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.AscendingTimestampExtractor;
import org.apache.flink.streaming.api.functions.windowing.ProcessAllWindowFunction;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.triggers.Trigger;
import org.apache.flink.streaming.api.windowing.triggers.TriggerResult;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;
import redis.clients.jedis.Jedis;

import java.net.URL;

/**
 * @Authror: jcshen
 * @Date: 2021/1/30
 * @Version: 1.0
 * @Description: UV统计：独立访客数
 *               如果放到Redis，亿级别的用户ID（每个20字节），可能需要几GB到十几GB的内存，不划算；
 *                每来一条数据触发一次计算，然后在窗口计算的过程中连接Redis，取出位图中保存的状态，判断是否存在，进行计算
 *
 *               使用布隆过滤器统计UV：
 *               位图（bitmap）：1表示存在，0表示不存在；只需要统计有多少个1即可；
 *               hash值，对应偏移量；存在hash碰撞
 *               --位图大小的扩充；hash函数足够复杂
 */
public class UvWithBloomFilter {
    public static void main(String[] args) throws Exception {
        //创建执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        //读取数据，创建DataStream
        URL resource = UvWithBloomFilter.class.getResource("/data/UserBehavior.csv");
        DataStream<String> inputStream = env.readTextFile(resource.getPath());
        //转换为POJO,分配时间戳盒watermark
        DataStream<UserBehavior> dataStream = inputStream
                .map(line -> {
                    String[] fields = line.split(",");
                    return new UserBehavior(new Long(fields[0]), new Long(fields[1]), new Integer(fields[2]), fields[3], new Long(fields[4]));
                })
                .assignTimestampsAndWatermarks(new AscendingTimestampExtractor<UserBehavior>() {
                    @Override
                    public long extractAscendingTimestamp(UserBehavior element) {
                        return element.getTimestamp() * 1000L;
                    }
                });
        //开窗统计uv值
        DataStream<PageViewCount> uvStream = dataStream
                .filter(data -> "pv".equals(data.getBehavior()))
                .timeWindowAll(Time.hours(1))
                .trigger(new UvTrigger())//自定义触发器
                .process(new UvCountWithBloomFilter());

        uvStream.print();

        env.execute("uv count with bloom filter job");
    }

    /**
     * 实现自定义触发器
     */
    public static class UvTrigger extends Trigger<UserBehavior, TimeWindow> {

        @Override
        public TriggerResult onElement(UserBehavior element, long timestamp, TimeWindow window, TriggerContext ctx) throws Exception {
            //每来一条数据，直接触发窗口计算，并直接清空窗口
            return TriggerResult.FIRE_AND_PURGE;
        }

        @Override
        public TriggerResult onProcessingTime(long time, TimeWindow window, TriggerContext ctx) throws Exception {
            //啥也不干
            return TriggerResult.CONTINUE;
        }

        @Override
        public TriggerResult onEventTime(long time, TimeWindow window, TriggerContext ctx) throws Exception {
            //啥也不干
            return TriggerResult.CONTINUE;
        }

        //清理状态
        @Override
        public void clear(TimeWindow window, TriggerContext ctx) throws Exception {

        }
    }

    /**
     * 自定义布隆过滤器
     */
    public static class UvBloomFilter{
        //定义位图的大小，一般需要定义为2的整次幂
        private Integer cap;

        public UvBloomFilter(Integer cap){
            this.cap=cap;
        }
        //实现一个hash函数
        public Long hashCode(String value,Integer seed){
            Long result = 0L;
            for (int i = 0; i < value.length(); i++) {
                result = result * seed + value.charAt(i);
            }
            //scala.util.hashing.MurmurHash3
            return result & (cap -1);//位移
        }
    }

    /**
     * 实现自定义的处理函数
     */
    public static class UvCountWithBloomFilter extends ProcessAllWindowFunction<UserBehavior,PageViewCount,TimeWindow> {

        //定义jedis连接和布隆过滤器
        Jedis jedis;
        UvBloomFilter uvBloomFilter;

        @Override
        public void open(Configuration parameters) throws Exception {

            jedis = new Jedis("localhost",6379);
            uvBloomFilter = new UvBloomFilter(1<<29);//对1亿userId去重，用64MB大小的位图
        }

        @Override
        public void process(Context context, Iterable<UserBehavior> elements, Collector<PageViewCount> out) throws Exception {
            //将位图和窗口count值全部存入Redis，windowEnd作为key
           Long windowEnd = context.window().getEnd();
            String bitmapKey = windowEnd.toString();
            //把count值存成一张hash表
            String countHashName = "uv_count";
            String countKey = windowEnd.toString();
            //1、取当前的userId
            Long userId = elements.iterator().next().getUserId();
            //2、计算位图中的offset
            Long offset = uvBloomFilter.hashCode(userId.toString(), 61);//质数
            //3、用Redis的getbit命令，判断对应位置的值
            Boolean isExist = jedis.getbit(bitmapKey, offset);
            //4、
            if (!isExist){
                //如果不存在，对应位图位置为1
                jedis.setbit(bitmapKey,offset,true);
                //更新Redis中保存的count值
                Long uvCount = 0L;//初始countzhi
                String uvCountString = jedis.hget(countHashName, countKey);
                if (uvCountString !=null && !"".equals(uvCountString)){
                    uvCount = Long.valueOf(uvCountString);
                }
                jedis.hset(countHashName,countKey,String.valueOf(uvCount + 1));
                //输出
                out.collect(new PageViewCount("uv",windowEnd,uvCount + 1));
            }
        }

        @Override
        public void close() throws Exception {
            jedis.close();
        }
    }

}
