package com.org.businessanalyse.networkflow_analisys;

import com.org.businessanalyse.hotitems_analisis.beans.UserBehavior;
import com.org.businessanalyse.networkflow_analisys.bean.PageViewCount;
import org.apache.flink.api.common.functions.AggregateFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.AscendingTimestampExtractor;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

import java.net.URL;
import java.util.Random;

/**
 * @Authror: jcshen
 * @Date: 2021/1/30
 * @Version: 1.0
 * @Description:    PV统计：解决数据倾斜问题
 *                   1、env.setParallelism(4)：设置并行度
 *                   2、new Tuple2<>("pv", 1L)：
 *                   key的均匀分布；这里是单个key，即使设置了并行度也只有一个分区
 *                   3、timeWindow(Time.hours(1))：本质是window all，只有一个窗口
 *
 */
public class PageViewOptimize {
    public static void main(String[] args) throws Exception {
        //创建执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(4);
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        //读取数据，创建DataStream
        URL resource = PageViewOptimize.class.getResource("/data/UserBehavior.csv");
        DataStream<String> inputStream = env.readTextFile(resource.getPath());
        //转换为POJO，分配时间戳盒watermark
        DataStream<UserBehavior> dataStream = inputStream
                .map(line -> {
                    String[] fields = line.split(",");
                    return new UserBehavior(new Long(fields[0]), new Long(fields[1]), new Integer(fields[2]), fields[3], new Long(fields[4]));
                })
                .assignTimestampsAndWatermarks(new AscendingTimestampExtractor<UserBehavior>() {
                    @Override
                    public long extractAscendingTimestamp(UserBehavior element) {
                        return element.getTimestamp()*1000L;
                    }
                });
        //分组开窗聚合，并行度任务改进，设计随机key，解决数据倾斜问题
        DataStream<PageViewCount> pvStream = dataStream
                .filter(data -> "pv".equals(data.getBehavior()))//过滤pv行为
                .map(new MapFunction<UserBehavior, Tuple2<Integer, Long>>() {
                    @Override
                    public Tuple2<Integer, Long> map(UserBehavior value) throws Exception {
                        Random random = new Random();
                        return new Tuple2<>(random.nextInt(10), 1L);
                    }
                })
                .keyBy(data -> data.f0)//按key分组
                .timeWindow(Time.hours(1))//开1小时滚动窗口
                .aggregate(new PvCountAgg(), new PvCountResult());
        //将各分区数据汇总起来，会出现相同key的同一个窗口有多次输出的问题，每来一次数据，就会计算一次
//        DataStream<PageViewCount> pvResultStream = pvStream.keyBy(PageViewCount::getWindowEnd).sum("count");
        //改进：让同一个窗口只有一次输出，把所有的数据收集齐了，再计算一次，进行输出
        DataStream<PageViewCount> pvResultStream = pvStream
                .keyBy(PageViewCount::getWindowEnd)
                .process(new TotalPvCount());//需要注册定时器
        //打印
        pvResultStream.print();
        //执行
        env.execute("page pv job");
    }

    /**
     * 实现自定义预聚合函数
     */
    public static class PvCountAgg implements AggregateFunction<Tuple2<Integer,Long>,Long,Long> {
        @Override
        public Long createAccumulator() {
            return 0L;
        }

        @Override
        public Long add(Tuple2<Integer, Long> value, Long accumulator) {
            return accumulator + 1;
        }

        @Override
        public Long getResult(Long accumulator) {
            return accumulator;
        }

        @Override
        public Long merge(Long a, Long b) {
            return a + b;
        }
    }

    /**
     * 实现自定义窗口
     */
    public static class PvCountResult implements WindowFunction<Long,PageViewCount,Integer, TimeWindow> {
        @Override
        public void apply(Integer integer, TimeWindow window, Iterable<Long> input, Collector<PageViewCount> out) throws Exception {
            out.collect(new PageViewCount(integer.toString(),window.getEnd(),input.iterator().next()));
        }
    }

    /**
     * 实现自定义处理函数，把相同窗口分组统计的count值叠加
     */
    public static class TotalPvCount extends KeyedProcessFunction<Long,PageViewCount,PageViewCount> {
        //定义状态，保存当前的总count值
        ValueState<Long> totalCountState;

        @Override
        public void open(Configuration parameters) throws Exception {
            totalCountState=getRuntimeContext().getState(new ValueStateDescriptor<Long>("total-count",Long.class,0L));//初始值为0
        }

        @Override
        public void processElement(PageViewCount value, Context ctx, Collector<PageViewCount> out) throws Exception {
            //每来一条数据后，取 value的count数量叠加到 totalCountState 里面
            //在之前的基础上提取 totalCountState 的值，叠加到 value 的count值，进行状态的更新
            totalCountState.update(totalCountState.value() + value.getCount());
            //注册定时器：当前的windowEnd 加上 1 毫秒
            ctx.timerService().registerEventTimeTimer(value.getWindowEnd() + 1);
        }

        //定时器触发，所有分组count值都到齐，直接输出当前的总count数量
        @Override
        public void onTimer(long timestamp, OnTimerContext ctx, Collector<PageViewCount> out) throws Exception {
            //当前totalCount，直接从 totalCountState 中拿出
            Long totalCount = totalCountState.value();
            //进行输出
            out.collect(new PageViewCount("pv",ctx.getCurrentKey(),totalCount));
            //清空状态
            totalCountState.clear();
        }
    }
}
